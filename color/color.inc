<?php

global $base_path, $user;

$theme_path = drupal_get_path('theme', 'bilderblock');

// Live preview settings.

// Prepare stylesheets for dynamic injection into preview iframe.
$preview_style_stubs = array(
  'core-modules' => "${base_path}modules/",
  'theme' => $base_path . $theme_path . '/css/',
);
$preview_styles = array(
  'core-modules' => array(
    'system/system.base.css',
    'system/system.menus.css',
    'system/system.messages.css',
    'system/system.theme.css',
    'field/theme/field.css',
    'node/node.css',
    'locale/locale.css',
  ),
  'theme' => array(
    'style.css',
    'drupal.css',
    'menu.css',
    'menu_hover.css',
    'navbar.css',
    'menu.css',
    'colors.css',
  ),
);
$css = array();
foreach ($preview_styles as $stub => $styles) {
  foreach($styles as $style) {
    $css[] = $preview_style_stubs[$stub] . $style;
  }
}

$settings = array(
  'exposed' => $base_path . $theme_path . '/color/fancy_exposed.png',
  'fancy' => $base_path . $theme_path . '/color/fancy.png',
  // Actual preview lives in an iframe to compensate standard preview downsides.
  // We may safely rely on JS, since there is no degradation process for color
  // preview anyway.
  'previewSource' => $base_path . $theme_path . '/color/preview_inner.html',
  'previewData' => array(
    'css' => $css,
    'i18n' => array(
      'html' => array(
        '#preview-author label' => t('By'),
        '#preview-author a.username' => check_plain($user->name),
        '#preview-comment-link' => t('Example comment link'),
        '#preview-content-header' => t('Example standard content'),
        '#preview-content-info' => t('This is how normal blocks will look when placed in the standard content region.'),
        '#preview-date label' => t('Date'),
        '#preview-date span' => format_date(REQUEST_TIME, 'short'),
        '#preview-example-term-1' => t('Example term 1'),
        '#preview-example-term-2' => t('Example term 2'),
        'h2 > a[href=#preview-exposed]' => t('Example slide'),
        '#preview-site-name' => check_plain(variable_get('site_name')),
        '#preview-menu-main-header' => t('Main menu'),
        '#preview-menu-secondary-header' => t('Secondary menu'),
        '.preview-menu-item#item-1-link' => t('Menu link 1'),
        '.preview-menu-item#item-2-link' => t('Menu link 2'),
        '.preview-menu-item#item-3-link' => t('Menu link 3'),
        '#preview-slogan' => check_plain(variable_get('site_slogan')),
        '.preview-tab-item.active' => t('Tab (active)'),
        '.preview-tab-item:not(.active)' => t('Tab'),
        '#preview-menu-block-header' => t('Menu block'),
        '#page-title' => t('bilderblock color preview'),
        'div#breadcrumb > div.breadcrumb > a' => t('Breadcrumb'),
        'div.meta.submitted span' => t('Submitted by nobody, never.'),
        '.preview-node-link' => t('Node link'),
      ),
      'title' => array(
        '#preview-comment-link' => t('There is no need to comment on everything'),
        'a[href=#preview-exposed]' => t('Show post'),
        '#preview-menu-link' => t('Show menu'),
        '.preview-node-link' => t('This is how a standard node link would look like'),
        '#preview-author a.username' => t("In a more realistic world, this link would lead to the author's profile."),
      ),
    ),
  ),
);
// Include color template, if color is enabled.
if (module_exists('color')) {
  $colors_template = DRUPAL_ROOT . $base_path . $theme_path . '/css/colors.css';
  if (
    (is_readable($colors_template))
    &&
    ($colors_template = file_get_contents($colors_template))
  ) {
    $settings['colorTemplate'] = $colors_template;
    $settings['colorTemplateWrapper'] = '<!--/*--><![CDATA[/*><!--*/
      !styles
    /*]]>*/-->';
  }
}
// Determine current logo path.
require_once $theme_path . '/template.php';
$settings['logo'] = array(
  'default' => $base_path . $theme_path . '/logo.png',
  // May be identical, this is accepted (form saving is required).
  'custom' => bilderblock::logoURL(),
);

drupal_add_js(array('color' => $settings), 'setting');

$info = array(
  // Available colors and color labels used in theme.
  'fields' => array(
    'global_background_color' => t('Global background'),
    'global_text_color' => t('Global text'),
    'content_background_color' => t('Content background'),
    'content_text_color' => t('Content text and links'),
    'content_link_visited' => t('Content link (visited)'),
    'content_input_background_color' => t('Form elements and comments background'),
    'content_input_border_color' => t('Form elements border'),
    'form_focus_border' => t('Form elements border (focused)'),
    'form_focus_background' => t('Form elements background (focused'),
    'film_strip_background' => t('Film strip background'),
    'film_strip_text' => t('Film strip text'),
    'film_strip_link_visited' => t('Film strip visited link'),
    'post_image_border' => t('Exposed image border'),
  ),
  // Pre-defined color schemes.
  'schemes' => array(
    'default' => array(
      'title' => t('Contact sheets (default)'),
      'colors' => array(
        'global_background_color' => '#1d1b16',
        'global_text_color' => '#fefefe',
        'content_background_color' => '#ccccbb',
        'content_text_color' => '#555544',
        'content_link_visited' => '#999988',
        'content_input_background_color' => '#ddddcc',
        'content_input_border_color' => '#333322',
        'form_focus_border' => '#fede00',
        'form_focus_background' => '#eeeedd',
        'film_strip_background' => '#000000',
        'film_strip_text' => '#7eb937',
        'film_strip_link_visited' => '#4e7322',
        'post_image_border' => '#ffffff',
      ),
    ),
  ),

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => array(
    'css/colors.css',
  ),

  // Files to copy.
  'copy' => array(
    'logo.png',
  ),

  // Gradient definitions.
  'gradients' => array(),

  // Color areas to fill (x, y, width, height).
  'fill' => array(),

  // Coordinates of all the theme slices (x, y, width, height)
  // with their filename as used in the stylesheet.
  'slices' => array(),

  // Reference color used for blending. Matches the base.png's colors.
  'blend_target' => '#ffffff',

  // Preview files.
  'preview_css' => 'color/preview.css',
  'preview_js' => 'color/preview.js',
  'preview_html' => 'color/preview.html',

  // Base file for image generation.
  'base_image' => 'color/base.png',
);
