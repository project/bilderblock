/**
 * @file
 * Additional color functionality for bilderblock theme.
 */

(function ($) {
  Drupal.color = {

    inited: false,

    callback: function(context, settings, form, farb, height, width) {
      // Apply init functions.
      if (!Drupal.color.inited) {
        $('#preview', context).once('bilderblock-preview', Drupal.color.init);
        Drupal.color.inited = true;
      }
      else {
        Drupal.color.updateColors();
        Drupal.color.updateSettings();
      }
    }, // Drupal.color.callback

    /**
     * Returns a handle for the actual iframe preview document.
     *
     * @return object
     */
    iFrame: function(sizzle) {
      sizzle = sizzle || 'html';
      return $('#preview iframe').contents().find(sizzle);
    }, // Drupal.color.iFrame

    /**
     * Initial preview setup tasks.
     *
     * Replaces dummy content with properly localized stuff like
     * translations, formatted dates and proper style sheet paths.
     */
    init: function() {
      $(this).find('.placeholder').remove();
      $('<iframe/>').attr('src', Drupal.settings.color.previewSource).appendTo($(this)).load(function() {
        // Inject stylesheets. This cannot be done in the iframe source,
        // since we never know the actual theme path in advance.
        $.each(Drupal.settings.color.previewData.css, function() {
          $('<link/>')
            .attr('type', 'text/css')
            .attr('rel', 'stylesheet')
            .attr('media', 'all')
            .attr('href', this)
            .appendTo(Drupal.color.iFrame('html head'));
        });

        // Create dynamic styles containers.
        $.each(['preview-inline-styles', 'preview-logo-styles'], function() {
          $('<style/>')
            .attr('id', this)
            .attr('type', 'text/css')
            .attr('media', 'all')
            .appendTo(Drupal.color.iFrame('html head'));
        });

        // Prepare dynamic logo toggling.
        Drupal.color.iFrame('#preview-logo-styles')
          .html(Drupal.formatString(Drupal.settings.color.colorTemplateWrapper, {
            '!styles': Drupal.formatString('body.bilderblock-logo #menu-logo:not(.logo-custom) > a:before {content:url(!default);} body.bilderblock-logo #menu-logo.logo-custom > a:before {content:url(!custom);}', {
              '!default': Drupal.settings.color.logo.default,
              '!custom': Drupal.settings.color.logo.custom
            })
          }));


        // Inject translations etc.
        $.each(Drupal.settings.color.previewData.i18n.html, function(selector, val) {
          var element = Drupal.color.iFrame(selector);
          if (element) {
            element.html(val);
          }
        });
        $.each(Drupal.settings.color.previewData.i18n.title, function(selector, val) {
          var element = Drupal.color.iFrame(selector);
          if (element) {
            element.attr('title', val);
          }
        });

        // Disable preview links.
        Drupal.color.iFrame('a:not([href=#preview-exposed])').click(function(event) {
          event.preventDefault();
        });
        Drupal.color.updateColors();
        Drupal.color.updateSettings();
      });
      $(this).addClass('active clearfix');

      // Track toggle switch events.
      $('form#system-theme-settings [name]')
        .filter('[name^=toggle_], [name^=default_], [name^=labels]')
        .change(Drupal.color.updateSettings);

      // Hack the color form to escape its meaningless 50em width prison.
      var colorForm = $(this).parent('div.color-form');
      var previewContainer = $('<div/>').attr('id', 'form-preview-wrapper').insertAfter(colorForm);
      $(this).siblings('h2').appendTo(previewContainer);
      $('<div/>')
        .appendTo(previewContainer)
        .html(Drupal.t('This advanced live preview reflects your color settings in realtime, and is also fluid/responsive. Try it by resizing your browser window and check your color settings across breakpoints.'));
      $(this).appendTo(previewContainer);
    }, // Drupal.color.init

    /**
     * Imitates the server-side color parsing, using the original colors.css.
     */
    updateColors: function() {
      var styles = Drupal.settings.color.colorTemplate;
      $("#palette input[name^='palette[']#").each(function() {
        var paletteId = $(this).attr('name').match(/\[(.*?)\]/);
        if (paletteId) {
          paletteId = paletteId[1];
          // map paletteId to color key in original document.
          var searchColor = Drupal.settings.color.schemes.default[paletteId];
          var regex = new RegExp(Drupal.formatString('^(\\s*)(\\b.+?\\b:\\s*)!searchColor;.*?$', {
            '!searchColor': searchColor
          }), 'gm');
          var replacement = Drupal.formatString('$1$2!color;', {
            '!color': $(this).val()
          });
          styles = styles.replace(regex, replacement);
        }
      });
      // Replace dynamic styles container content.
      Drupal.color.iFrame('#preview-inline-styles')
        .html(Drupal.formatString(Drupal.settings.color.colorTemplateWrapper, {
          '!styles': styles
        }));
    }, // Drupal.color.updateColors

    /**
     * Helper: get a form value.
     */
    getFormSetting: function(formName) {
      var el = $(formName.replace(/(.*)/, 'form#system-theme-settings [name="$1"]'));
      switch (el.attr('type')) {
        case 'checkbox':
          return el.is(':checked');
        case 'radio':
          return el.filter(':checked').val();
        default:
          return el.val();
      }
    }, // Drupal.color.getFormSetting

    /**
     * Synchronizes certain theme settings from the form to the preview area.
     */
    updateSettings: function() {
      Drupal.color.iFrame('#preview-author label').toggle(Drupal.color.getFormSetting('labels[by]'));
      Drupal.color.iFrame('#preview-date label').toggle(Drupal.color.getFormSetting('labels[date]'));
      Drupal.color.iFrame('#main-menu').toggle(Drupal.color.getFormSetting('toggle_main_menu'));
      Drupal.color.iFrame('#secondary-menu').toggle(Drupal.color.getFormSetting('toggle_secondary_menu'));
      Drupal.color.iFrame('#menu-logo').toggle(Drupal.color.getFormSetting('toggle_logo'));
      Drupal.color.iFrame('#menu-logo').toggleClass('logo-custom', !Drupal.color.getFormSetting('default_logo'));
      Drupal.color.iFrame('#menu-title .name').toggle(Drupal.color.getFormSetting('toggle_name'));
      Drupal.color.iFrame('#menu-title .slogan').toggle(Drupal.color.getFormSetting('toggle_slogan'));
      Drupal.color.iFrame('#menu-header')
        .toggleClass('preview-empty',
          !Drupal.color.getFormSetting('toggle_logo')
          &&
          !Drupal.color.getFormSetting('toggle_name')
          &&
          !Drupal.color.getFormSetting('toggle_slogan')
        );
      Drupal.color.iFrame('body').toggleClass('bilderblock-post-image-border', Drupal.color.getFormSetting('toggle_post_image_border'));
      Drupal.color.iFrame('#breadcrumb').toggle(Drupal.color.getFormSetting('toggle_breadcrumb'));


      //toggle_post_image_border
    } // Drupal.color.updateSettings
  };
})(jQuery);
