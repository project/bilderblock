BILDERBLOCK THEME
#################

Project page:  	http://drupal.org/project/bilderblock
Author:			[David Herminghaus](http://david.herminghaus.de)


ABSTRACT
--------

bilderblock is a Drupal photo blog theme. Its main purpose is to provide a
sensible, image-focused, maximally pluggable frontend to any standard-compliant
Drupal site.

I might however decide to publish a dedicated Drupal installation profile
with a meaningfull setup of modules, rules and views, but that is not part
of this introduction.

Check out the theme project page now and then, I will likely add some online
manuals to drupal.org when I find the time.

### Consider bilderblock, if...

* you are reluctant to share your images on one of the many existing
  "cloud" photo sharing services,
* you are already basically familiar with Drupal site building
  (you should at least have heard about the concepts of "image styles",
  "taxonomies" and probably "views"),
* you have a clear idea for your (photo) blog's content structures and
  are now looking for a clean, pluggable, cross-browser compliant
  frontend interface.

### Probably avoid bilderblock, if...

* you are completely unfamiliar with Drupal and not ambitious to change that,
* you are in a hurry and need a five-minute all-in-all somehow-somewhat solution
  (although setting bilderblock up is close to a picknick),
* you have hardly figured just how your blog is intended to work at all
  (because bilderblock is intentionally limited to display related tasks
  and will not provide any structural or otherwise content-focused ´
  functionality).

Unless the latter applies, find some basic information on bilderblock theme's
capabilities and drawbacks, and on how it integrates with Drupal, below.

FEATURES
--------

Since bilderblock does not intend to be a "controller view", it is free to
focus on original display features:

* Images first:
  Teasers render as slides within "strips", much like an oldschool analogue
  contact print; selected images in full node displays render as "exposed
  images" with an optional, also oldschoolish, border.
  The menu conditionally collapses into a slim navigation bar that always sticks
  to the least obtrusive border.
* Basic image zoom:
  An optional Javascript-free enlargement functionality for exposed images can
  be combined with advanced Javascript zoom libraries or completely disabled.
* Responsible and fluid:
  Layouts degrade in multiple steps down to very small screen display. Content
  dimensions adapt seamlessly to practically any viewport.
* Colorizable:
  Each defined color can be adjusted to your needs. Includes a responsive,
  realtime theme preview for colors and selected theme settings.
* Customizable:
  Add and configure CSS3 filter effects, hover functionality, menu and navbar
  behavior and even privacy settings.
* Pluggable:
  Integrates with basically all Drupal core functions, such as image styles,
  fields, i18n (see also the list below).
* Fully degradable:
  Javascript is not required for any frontend component. Menu and navbar do use
  some jQuery improvements, but also degrade to fully functional fallbacks.
  Even unstyled markup is still readable and contains no "hack" elements,
  except for the optional "image zoom" checkbox.

BASIC INSTALLATION
------------------

bilderblock has so far been tested working with multiple rather sophisticated
setups, and also with a "vanilla" Drupal (one that has been freshly installed
from official sources). As seen from the latter one, here is an example
bilderblock photo blog setup guide:

1. (Con)figure your basic site structures:

   1.1 Image styles
       
       bilderblock does not perform any image processing, but accepts any
       provided standard Drupal content for rendering.
       
       1. Teaser slides
          
          In order to have realistic "film strips" with a consistent height,
          decide for a fixed slide size first. Then, create an image style
          that forces this height.
       
          * For a strip of squared slides with fixed dimensions of 320x320
            pixels, you would set up "scale & crop 320 x 320".
          * For a strip of slides with 320px height and 480px width,
            you would set up "scale & crop 480 x 320".
       
          bilderblock's layouts break at multiples of 320px width, with images
          conditionally auto-fitting a maximum of 320 pixels. Therefore, it is
          recommended to also make sure that your images will not need
          upscaling.
          
          See also "known issues" below.
          
       2. Exposed image (optional)
       
          The "full node" display is indifferent of the provided image(s),
          however you may want to set up an appropriate style anyway.
       
       You may also want to consider using server-side image effects instead
       of relying on the CSS3 client-side filters provided with the theme,
       in case you want to guarantee a certain image output instead of relying
       on the client's browser. In this case, check out the available Drupal
       image style effect plugins.
       
   1.2 Content type display settings
   
       Arrange your field display settings. bilderblock will just render
       whatever your site configuration provides, so this is an important step.
       
       For any image field that is supposed to render as a slide/exposed image,
       make sure you select your image style from 1.1 in the display settings.
       
       bilderblock will render whatever fields you configure in the node type
       display settings, with or without labels (see also theme settings
       section).
       
       Here is where enabled node fields will appear:
       
       1. "teaser" display
          
          A single node teaser has the following structure:
          
          1. Upper "side-strip" (film strip section above the slide):
       
             1. Node title
             2. Node author
             3. Node links (e. g. "add comment")
             
          2. Actual slide content:
       
             The first image of any image field that has been enabled in the
             theme settings (see below), following the configured field look-up
             priorities.
             
             If a node does not have any images in one of the configured fields,
             or if a node type does not even have one of the configured fields
             attached, or if no field has been configured, or if there are no
             image fields at all, bilderblock will use a fallback image
             (actually cat content). See below for cat-haters.
             
          3. Lower "side-strip" (section below the slide):
       
             1. "Created" date
                The node's creation time stamp, rendered in whatever format
                you have set up in your date/time settings as "short".
             
             2. Any other fields configured in your teaser display will render
                here, such as term references, except for "body" (this is
                a photo blog theme; "body" will be ignored in teasers).
       
       2. "default" (full node) display
       
          1. Upper "exposed image" region
          
             The first available image from the configured image sources
             will be moved from its original content context to this region
             and render with or without a "border" frame, depending on the
             theme settings.
          
          2. Standard content section
          
             This section will render all other configured display content,
             if available, in order of this listing:
             
             1. Standard "body" content
             2. Any other content (except comments and links)
             3. Node meta information (author, creation date etc.)
             4. Node links (translations, comments etc.)
             5. Comments form/existing comments

   1.3 Image field placeholders (optional)
       
       In case you dislike cat placeholder content, you may want to simply
       configure a default picture of your choice for any related image field.
       Note that, in this case, the configured image will also show
       in the full node display.
       
       You may instead want to upload your own dedicated teaser slide fallback
       image (see 3.).
    
2. Enable the theme.

3. Configure the theme.

   Most of the theme settings are self-explanatory (if not, kindly file an
   issue). Here is an overview of what you can actually configure:
   
   * Theme colors
     You will likely want to change the default color schema. You may
     control every color group actually used in the theme and see the
     results immediately in the full-realtime preview section.

   * Slide effects
     The default settings include a slight ("lightbox"-style) brightness
     effect, you can control teaser effects in many aspects.
     Remember that all CSS3 image effects depend on your visitors' browsers.
     See "Slides / Slide effects".
   
   * Custom fallback slide
     To add your own (or replace the existing) cat content, you may as well
     upload a dedicated fallback slide image.
     See "Slides / Fallback slide settings".
   
   * Exposed image functions
     Configure zoom and decorative border options, or disable automatic
     detection.
     See "Exposed images".
     
     Configure and priorize the image fields too browser for automatic
     detection of sources for exposed images (and slides in this case).
     At least one image field should be enabled for obvious reasons,
     by default, this applies to the standard "field_image".
   
   * Menu behavior
     Menu and navbar can be adjusted, e. g. to always be collapsed or to
     react to certain mousover ("hover") events.
     See "General display / Menu".
     
     By default, each user's menu state is tracked locally with a cookie.
     You may want to disable this for caching considerations or in order
     to respond to privacy requirements.
     See "Advanced settings".
     
   * Hover effects
     Menu and slide hover effects depend on mouse availability and are little
     helpful on touch display. You can configure under which conditions hover
     events will be tracked at all (this overrides more particular settings).
     Note the built-in "modernizr" support, too.
     See "General display".

   * Label display
     Native node fields ("by" and "date") cannot be toggled in the content
     type display settings. See "Slides / Film strip labels".
   
   Please note the known issues at the end of the file for limitations.


DRUPAL INTEGRATION
------------------

Here are some more notes on how bilderblock integrates with existing Drupal
components.

1. Drupal core

   * Front page and taxonomy
     Front page and taxonomy term pages work out of the box and will display
     "film strips" from slides as of your display and image style settings.
   
   * Localization
     bilderblock consequently uses Drupal translation, even for the theme
     settings preview. Find any translatable string in Drupal's translation
     backend.
     You may want to consider supporting the i18n team with an official 
     bilderblock translation in your language.

2. Contributed modules
   
   * Adaptive image styles
     Works pretty unattended, however, double-check in order to avoid upscaling.
   
   * Admin menu (toolbar)
     bilderblock recognizes an existing admin menu toolbar, and will adjust its
     spacing. This, however, uses hard-wired criteria, and may fail under
     certain conditions.
   
   * Colorbox
     Just set up your teaser display as usual.
   
   * Field groups
     Helpful if you want to arrange certain node fields within additional
     structures. See below for some generic CSS helper classes provided
     by the theme.
   
   * Fontyourface
     The fontyourface module simplifies the use of additional (web) fonts.
     This works so simple that it needs no description.
     
     However, for advanced theming tasks, the theme suggests two "typeface
     regions" with different content:
     
     * .bilderblock-font-global
       This class is applied to the body and to any class that is supposed
       to use the site's basic font family.
     
     * .bilderblock-font-slide
       Covers film strips and slides, which are best off with a narrow,
       clean and serifeless face.
     
   * Fontawesome
     If this module is enabled, bilderblock will use fontawesome icons
     instead of the standard unicode symbols and the standard home icon
     bitmap (the standard unicode set does not provide anything close to
     a "home").
     Consider using fontawesone if you do not want to upload a custom
     "home" icon simply for colorization reasons, or simply if you like
     to have more beautiful icons.
   
   * Modernizr
     bilderblock recognizes modernizr's "html.touchevents" class to decide
     whether hover-based effects will apply or not (see the corresponding
     theme setting at "General display".
   
   * Region View Modes
     Nice module that allows for rendering node fields in a block region
     of your choice. Helpful if you want to make native use of the
     dedicated "teaser slides" and "exposed image" block regions.
     Instantly working.
   
   * Views
     In order for views to provide teaser slides, choose "content" as row
     style format. To denote a non-standard section as "contains teasers",
     you may use the
     
     .bilderblock-has-teasers
     
     class. It will force any block to render similar to the dedicated
     teaser slide region (i. e. with different spacing and transparent
     background).

   * Views infinite scroll
     Cool extension for lazy-loading paged views, brings your site close
     to one of these public "photo streams". Integrates out of the box.

3. Generic CSS helpers

For use with e.g. field_groups or other customizations, bilderblock provides
some generic helper classes:

* .nomargin, .nopadding
  Remove all margins (resp. paddings) from an element.

* .noborders
  Remove all border definitions from an element.

* .display-inline, .display-inline-block, .display-block
  Force an element to display as inline/inline-block/block.

* .img-autofit
  Forces an element and any of its probably contained images
  to proportionally adjust to the current page width.

Note that these definitions usually contain an "!important" rule, since
they are considered "last rescue" helpers vor very high-level tasks.
Use them at your own risk.

You can find these helpers at the very bottom of css/style.css


EXPOSED IMAGES
--------------

Exposed images are rendered in a dedicated, priorized theme region.
This is achieved in various ways:

1. DEFAULT PROCESS
   
   By default, the theme searches the node for an existing image in
   one of the fields configured as "image sources" in the theme settings.
   This image is moved from its original content context to the
   "exposed" region.

2. CUSTOM APPLIANCE
   
   There are also ways to force certain content to be rendered as "exposed".

   1. By region
      
      Put any image into a block and make that block render in the
      "exposed" region.
      Or use the [Region View Modes](https://www.drupal.org/project/region_view_modes)
      module to do the same with image fields attached to your nodes.

   2. By CSS classes
      
      You can also create an exposed-style section anywhere else, by wrapping
      images into some container elements, e. g. when using the field_group
      module. Example for all-manual markup (need not be <div/> elements):
      
      <div class="bilderblock-image-outer">
      	<div class="bilderblock-image-container">
      		<!-- your actual content here -->
      	</div>
      </div>

      Note that this second option will only provide the markup required for
      the optional exposed image pseudo "border". It will not enable the
      basic image zoom functions.

   You may want to disable theme-internal processing of exposed images in the
   theme settings, see "Exposed images".
   
   The "exposed image" "pseudo border" feature avoids native CSS borders, for
   they tend to mess up CSS box models. Instead, a structure of nested divs
   is applied.

FILM STRIPS / TEASER SLIDES
---------------------------
Search results and taxonomy term pages will automatically render as film
strips with node teasers. This will make sure that the block/content will not be
displayed with a colorized background, and also with a "film strips" layout.

Still, you may create custom film strip sections, too:

1. By region

Place any block or other element containing teasers into the theme's
"teasers" region. (Avoid placing standard content there,  it will
render quite unpleasantly.)

2. By CSS classes

Use the "bilderblock-has-teasers" class for blocks that contain such teaser
slides. In certain cases, you may need to set your views display class to 
"bilderblock-has-teasers".

KNOWN ISSUES
------------
The theme is still work in progress. Check the issue queue first
for recent updates.

### Theme settings preview
The preview section has been tweaked a little, compared to standard practice,
in order to provide a realistic mock-up and to avoid the usual duplicate code,
since the latter is a real common source of annoyance with a lot of colorizable
themes.

bilderblock uses an iframe for the preview section, and some dynamic
interaction with that iframe. By all advantages, this comes at the downside of
some older IE versions not properly rendering the preview section.
Since the color preview is an advanced backend-only feature, I decided to
declare this an accepted limitation. Kindly try a different browser for your
color settings, if you encounter IE issues. Thanks for your understanding,
I really do dislike such limitations in general.

### CSS3 effects
This is actually not an issue, just a reality-check. bilderblock provides some
fance CSS3 effects. None of them has actually made it into official standards
so far, however, most of them are de-facto standards since a couple of years
now. Still there is no guarantee that all effects work in all browsers. To
verify your effect settings, a recent Chrome or Firefox is fine enough.

The good news is, however, that CSS3 effects degrade gracefully and without
damage. Your audience will simply see the original image (style) in the worst
case.

### Variable teaser slide widths
Currently, variable widths are basically feasible, but render the responsive
layout a bit chaotic. I will investigate integration options with "mansonry"
at a later step.
