<?php

/**
 * @file
 * Main toolset class for bilderblock theme.
 */

class bilderblock {

  // Track and cache valid source fields.
  private static $validFields;

  // Track and cache image style/field instance mapping.
  private static $imageStyles = array();

  // Track and cache fallback image data.
  private static $fallback = array();

  // Cache label defaults and settings for node teasers.
  private static $labelDefaults = array();
  private static $labels;

  // Cache image size class-per-uri mappings.
  private static $imageSizes = array();

  // Cache image size classes.
  private static $imageSizeClasses = array();

  const BREAKPOINT_DESKTOP_MIN = 961;
  const CSS_CACHE_VAR = '_preprocessed';
  const CSS_HASH_VAR = '_css_hash';
  const HOVER_NEVER = 0;
  const HOVER_DESKTOP_NOTOUCH = 1;
  const HOVER_ALWAYS = 2;
  const SVG_MATRIX_VAR = '_svg_matrix';
  const ZOOM_ALWAYS = 0;
  const ZOOM_FALLBACK = 1;
  const ZOOM_NEVER = 2;

  /**
   * Checks whether the dynamic CSS template has changed.
   *
   * Compares the dynamic CSS template to the last-seen version
   * and returns whether there has been an update.
   *
   * @return bool
   */
  public static function cssUpdated() {
    return
      (!$css = theme_get_setting(bilderblock::CSS_HASH_VAR, 'bilderblock'))
      ||
      (
        ($includePath = self::cssTemplatePath())
        &&
        ($template = file_get_contents($includePath))
        &&
        ($css != md5($template))
      )
    ;
  } // cssUpdated()

  /**
   * Preprocesses and caches dynamic CSS.
   *
   * A number of theme settings cannot be mapped directly by CSS, as they contain
   * dynamic variables. To reduce the overhead of inline CSS generation and
   * injection, these dynamic settings are created from a template and cached.
   *
   * @param string $force
   *   Whether to ignore an already existing file.
   *
   * @return string
   *   The actual dynamic CSS (if available).
   *
   * @see bilderblock::clearCaches()
   * @see inc/dynamic_styles.inc
   */
  public static function css($force = FALSE) {
    if (
      ($force)
      ||
      // Empty value indicates that the form has been updated
      // (there is no value transport for this setting by intention).
      // In this case, the theme settings are empty.
      (!$css = theme_get_setting(bilderblock::CSS_CACHE_VAR, 'bilderblock'))
    ) {
      // Generate the CSS.
      if ($include_path = self::cssTemplatePath()) {
          ob_start();
          include $include_path;
          // No need for strict FALSE check as 0 bytes would indicate an issue, too.
          if ($raw = ob_get_clean()) {
            $css = $raw;
            // Update theme settings.
            $ts = variable_get('theme_bilderblock_settings', array());
          $ts[bilderblock::CSS_CACHE_VAR] = $css;

          // Create md5 hash of unprocessed template to track changes.
          $templateRaw = file_get_contents($include_path);
          $ts[bilderblock::CSS_HASH_VAR] = md5($templateRaw);

            variable_set('theme_bilderblock_settings', $ts);
          }
        }
      }
    return $css;
  } // css()

  /**
   * Creates an SVG feColorMatrix string for custom "tint" CSS3 filter effect.
   *
   * @param string $paletteId
   *   Color palette identifier of the color to use for "tint".
   *
   * @return string|bool
   *   An feColorMatrix string or FALSE on errors.
   */
  public static function colorMatrix($paletteId) {
    if (
      (!$ret = theme_get_setting(bilderblock::SVG_MATRIX_VAR, 'bilderblock'))
      ||
      (!isset($ret[$paletteId]))
    ) {
      $ret[$paletteId] = FALSE; // Fallback/error-tracking
      if ($intensity = theme_get_setting('fx_tint', 'bilderblock')) {
        if ($palette = color_get_palette('bilderblock')) {
          if ($tint =@ $palette[$paletteId]) {
            // Dissect and normalize hex value.
            if ($hex = preg_replace('#[^a-f0-9]#i', '', $tint)) {
              $hex = preg_replace('#^(.)(.)(.)$#', '$1$1$2$2$3$3', $hex);
              if (preg_match('#^(?<r>..)(?<g>..)(?<b>..)$#i', $hex, $dec)) {
                array_walk($dec, function(&$color) {
                  // Normalize colors to "percent" (float).
                  $color = hexdec($color) / 255;
                });
                // Create a matrix from the calculated rgb color.
                // This matrix tries to map the relations "inside" the tint color
                // to the still unknown target color, while preserving the original
                // color channel (no shifting).
                // Target matrix design (simplified):
                /*
                1      [R-G]  [R-B]  0 0
                [G-R]  1      [G-B]  0 0
                [B-R]  [B-G]  1      0 0
                0      0      0      1 0
                */
                $matrix = array();
                foreach (array('r', 'g', 'b') as $src) {
                  foreach (array('r', 'g', 'b') as $dest) {
                    $matrix[] = $src == $dest ? 1 : ($dec[$src] - $dec[$dest]) * ($intensity / 100);
                  }
                  $matrix[] = 0;
                  $matrix[] = 0;
                }
                $ret[$paletteId] = implode(' ', $matrix) . ' 0 0 0 1 0';
              }
            }
          }
        }
      }
      // Update theme settings
      $ts = variable_get('theme_bilderblock_settings', array());
      $ts[bilderblock::SVG_MATRIX_VAR] = $ret;
      variable_set('theme_bilderblock_settings', $ts);
    }
    return $ret[$paletteId];
  } // colorMatrix()

  /**
   * Prepares an "exposed image" markup section.
   *
   * @param string $markup
   *   Prepared markup for the actual exposed image (by reference).
   * @param int $zoomMode
   *   One of the bilderblock::ZOOM constants for the "zoom_mode" theme setting.
   *
   * @return string
   *   The prepared markup.
   */
  public static function markupExposed($markup, $zoomMode) {
    $tooltip = t('Clicking here may enlarge the image to its full size.');
    $result = <<<EOT
      <div class="exposed-image clearfix">
        <div class="exposed-wrapper" title="${tooltip}">
          $markup
        </div><!--/.exposed-wrapper-->
      </div><!--/.exposed-image-->
EOT;

    if ($zoomMode != bilderblock::ZOOM_NEVER) {
      $result = bilderblock::markupImageZoom($result, $zoomMode);
    }
    $result = <<<EOT
      <div class="exposed-outer">
        $result
      </div><!--/.exposed-outer-->
EOT;
    return $result;
  } // markupExposed()

  /**
   * Adds the required wrappers to given markup.
   *
   * @param string $markup
   *   The markup to wrap.
   * @param string $zoomMode
   *   (Optional) overrides theme-wide zoom mode settings
   *   (unimplemented, but for better preprocess support).
   *
   * @return string
   *   The wrapped markup.
   */
  public static function markupImageZoom($markup, $zoomMode = NULL) {
    if (!isset($zoomMode)) {
      $zoomMode = theme_get_setting('zoom_mode', 'bilderblock');
    }
    if ($zoomMode != bilderblock::ZOOM_NEVER) {
      // Add zoom, if configured.
      $tooltip = t('Expand image to full size');
      // Create unique DOM ID for label.
      $id = drupal_html_id('zoom-image');
      $markup = <<<EOT
      <div class="imgzoom">
        <input type="checkbox" title="$tooltip" id="$id" />
        <label for="$id">
          $markup
        </label>
      </div><!--/.imgzoom-->
EOT;
    }
    return $markup;
  }

  /**
   * Helper for FAPI standard default/upload file component.
   *
   * @param string $childName
   *   Machine name identifier, e. g. 'home_icon'.
   * @param string $label
   *   Human readable name, e. g. "Home icon".
   * @param string $description
   *   General item description, used for the outermost container.
   *
   * @return array
   *   FAPI array for a file default/upload setting, representing
   *   the current element.
   */
  public static function formFileUpload($childName, $label, $description) {
    return array(
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#title' => t('%config_item settings', array(
        '%config_item' => $label,
      )),
      '#description' => $description,
      '#attributes' => array(
        'class' => array('theme-settings-bottom'),
      ),
      "default_$childName" => array(
        '#type' => 'checkbox',
        '#title' => t('Use the default %config_item', array(
          '%config_item' => $label,
        )),
        '#default_value' => theme_get_setting("default_$childName", 'bilderblock'),
        '#tree' => FALSE,
        '#description' => t('Check here if you want the theme to use the theme-provided %config_item.', array(
          '%config_item' => $label,
        )),
      ),
      'settings' => array(
        '#type' => 'container',
        '#states' => array(
          'invisible' => array(
            'input[name="default_' . $childName . '"]' => array(
              'checked' => TRUE,
            ),
          ),
        ),
        "{$childName}_path" => array(
          '#type' => 'textfield',
          '#title' => t('Path to custom %config_item', array(
            '%config_item' => $label,
          )),
          '#description' => t('The path to the file you would like to use instead of the default %config_item.'),
          '#default_value' => theme_get_setting("{$childName}_path", 'bilderblock'),
        ),
        "{$childName}_upload" => array(
          '#type' => 'file',
          '#title' => t('Upload %config_item', array(
            '%config_item' => $label,
          )),
          '#maxlength' => 40,
          '#description' => t("If you don't have direct file access to the server, use this field to upload your %config_item.", array(
            '%config_item' => $label,
          )),
        ),
      ),
    );
  } // formFileUpload()

  /**
   * Provides fallback image data suitable for the given node.
   *
   * @param object $node
   *   The node to find a fallback image for.
   * @param string &$class
   *   Will be filled with the corresponding image size class for templates.
   *
   * @return string
   *   Themed fallback image markup, if available.
   */
  public static function getFallbackImage($node, &$class) {
    if (!isset(self::$fallback[$node->type])) {
      $fallbackSource = array(
        'title' => t('This post has no exposed image'),
      );
      $class = FALSE;
      if ($fallbackStyle = bilderblock::getImageStyle($node)) {
        $sourcePath = bilderblock::resourceLocation('fallback_image');
        if ($imgPath = bilderblock::getStyledImagePath($sourcePath, $fallbackStyle)) {
          $fallbackSource['path'] = image_style_url($fallbackStyle, $sourcePath);
          if ($class = self::setImageSizeClass($imgPath, $imagesizes)) {
            $fallbackSource = array_merge($fallbackSource, array(
              'width' => $imagesizes[0],
              'height' => $imagesizes[1],
            ));
          }
        }
      }
      else {
        // Set very generic fallback if no image field is available to this type.
        $sourcePath = drupal_get_path('theme', 'bilderblock') . '/images/fallback_teaser.png';
        $fallbackSource = array_merge($fallbackSource, array(
          'path' => $sourcePath,
          'width' => 320,
          'height' => 320,
        ));
      }
      $fallbackSource = theme('image', $fallbackSource);

      self::$fallback[$node->type] = $fallbackSource;
      self::$imageSizeClasses['fallback'][$node->type] = $class;
    }
    $class = self::$imageSizeClasses['fallback'][$node->type];
    return self::$fallback[$node->type];
  }

  /**
   * Extracts and caches image dimensions and returns a style class.
   *
   * The theme will collect all dimensions types registered with this
   * function in order to pass them as inline CSS. The corresponding
   * class is returned.
   *
   * @param array $slide
   *   Render array for the actual slide.
   *
   * @return string
   *   An image size CSS class.
   */
  public static function getImageSizeClass($slide) {
    $uri = $slide[0]['#item']['uri'];
    if (!isset(self::$imageSizeClasses[$uri])) {
      $class = FALSE;
      if ($style =@ $slide[0]['#image_style']) {
        if ($path = image_style_path($style, $uri)) {
          $class = self::setImageSizeClass($path, $imagedata);
        }
      }
      self::$imageSizeClasses[$uri] = $class;
    }
    return self::$imageSizeClasses[$uri];
  }

  /**
   * Returns CSS mapping image size classes to w/h dimensions.
   *
   * @return string
   */
  public static function getImageSizeStyles() {
    $css = '';
    foreach (self::$imageSizes as $class => $dimensions) {
      if (!empty($dimensions)) {
        $css .= sprintf('.%s{width: %spx; height: %spx;}', $class, $dimensions[0], $dimensions[1]);
      }
    }
    return $css;
  }

  /**
   * Tries to retrieve image dimensions and defines a style class.
   *
   * @param string $path
   *   File path.
   * @param array &$dimensions
   *   By reference; filled with the getimagesize results.
   *
   * @return string|bool
   *   A css class on success, or FALSE.
   */
  private static function setImageSizeClass($path, &$dimensions) {
    $class = FALSE;
    if (is_readable($path)) {
      if ($dimensions =@ getimagesize($path)) {
        $class = "bilderblock-size-{$dimensions[0]}-{$dimensions[1]}";
        self::$imageSizes[$class] = $dimensions;
      }
    }
    return $class;
  }

  /**
   * Determines the exposed image style for a given node.
   *
   * Nodes without exposed image still need a fallback and a lowsrc
   * image in the same image style as a node with an image.
   * This function determines the appropriate image style to use
   * for the given node and display type.
   *
   * @param object $node
   *   The node to determine the image style for.
   * @param string $display
   *   (Optional) Name of the node display in question.
   *   Defaults to "teaser".
   *
   * @return string|bool
   *   Name of the image style to apply, of FALSE if none is found.
   */
  public static function getImageStyle($node, $display = 'teaser') {
    if ($type =@ $node->type) {
      if (!isset(self::$imageStyles[$type][$display])) {
        $styleName = FALSE;
        // Although image fields are empty, the node has still set their property:
        foreach (self::getValidFields() as $fieldname) {
          if (isset($node->{$fieldname})) {
            $instance = field_info_instance('node', $fieldname, $node->type);
            if (!empty($instance['display'][$display]) && !empty($instance['display'][$display]['settings'])) {
              if ($styleName =@ $instance['display'][$display]['settings']['image_style']) {
                break;
              }
            }
          }
        }
        self::$imageStyles[$type][$display] = $styleName;
      }
      return self::$imageStyles[$type][$display];
    }
    return FALSE;
  } // getImageStyle()

  /**
   * Get all locales of "by" and "date" labels.
   *
   * @return array
   *   Translated strings for the current language.
   */
  public static function getLabelDefaults() {
    return array(
      'by' => t('By'),
      'date' => t('Date'),
    );
  }

  /**
   * Get actually configured locales for "by" and "date" labels.
   *
   * @return array
   *   Zero or more translated strings for the current language,
   *   keyed by type ("by", "date").
   */
  public static function getLabels() {
    // Labels might be needed often during a request.
    if (!isset(self::$labels)) {
      self::$labels = self::getLabelDefaults();
      // Respect display configuration:
      foreach (theme_get_setting('labels', 'bilderblock') as $labelName => $labelValue) {
        if (empty($labelValue)) {
          unset(self::$labels[$labelName]);
        }
      }
    }
    return self::$labels;
  }

  /**
   * Delivers a derivative from an image source defined in theme settings.
   *
   * @param string $sourcePath
   *   Drupal path to the image source.
   * @param string $styleName
   *   Image style name.
   */
  public static function getStyledImagePath($sourcePath, $styleName) {
    if (module_exists('image')) {
      $targetPath = image_style_path($styleName, $sourcePath);
      if (
        (is_readable($targetPath))
        ||
        (
          ($style = image_style_load($styleName))
          &&
          (image_style_create_derivative($style, $sourcePath, $targetPath))
        )
      ) {
        return $targetPath;
      }
      else {
        drupal_set_message(t('%style derivative for %source could not be removed from and/or recreated at %target. This most likely indicates a generic file permission issue.', array(
          '%target' => $targetPath,
          '%source' => $sourcePath,
          '%style' => $styleName,
        )), 'error');
        watchdog(
          'bilderblock',
          '%style derivative for %source could not be removed and/or recreated at %target. This most likely indicates a generic file permission issue.',
          array(
            '%target' => $targetPath,
            '%source' => $sourcePath,
            '%style' => $styleName,
          ),
          WATCHDOG_ERROR
        );
      }
    }
    // Fall back to original source on errors.
    $localSourcePath = base_path() . $sourcePath;
    if (is_readable($localSourcePath)) {
      // Source location outside Drupal filesystem has no stream wrappers.
      return DRUPAL_ROOT . base_path() . $sourcePath;
    }
    return FALSE;
  } // getStyledImagePath()

  /**
   * Browses a node render array for image sources as of theme settings.
   *
   * @param array $node
   *   A node object.
   *
   * @return string|bool
   *   The name of the effective image field providing the exposed image,
   *   or FALSE if no image source is present or an error occured.
   */
  public static function imageSource($node) {
    foreach (self::getValidFields() as $fieldname) {
      if (!empty($node->{$fieldname})) {
        return $fieldname;
      }
    }
    return FALSE;
  } // imageSource()

  /**
   * Provides the path to the current actual site logo.
   *
   * Checks for the config state (default/custom) of any theme-configurable
   * image and returns the location for the (unstyled) resource.
   *
   * @param string $stub
   *   (Optional) the theme settings filename stub to process (e.g. "home_icon").
   *   Defaults to "logo". Intended to allow for looking up logo and home icon
   *   the same way.
   *
   * @return string|FALSE
   *   The public path to the logo (a URL), or FALSE on errors.
   */
  public static function logoURL($stub = 'logo') {
    // Other than Drupal's standard logo injection, our logos live in
    // pre-processed, cached CSS. Since we have no idea of which host(s)
    // will later request that preprocessed CSS, we must not use a bare
    // file_create_url (as it includes the current host in the result).
    // @see https://www.drupal.org/node/837794#comment-5967894
    if (
      ($url = self::resourceLocation($stub))
      &&
      ($url = file_create_url($url))
      &&
      ($url = parse_url($url))
    ) {
      return $url['path'];
    }
    return FALSE;
  } // logoURL()

  /**
   * Checks for valid exposed_image source field names.
   *
   * Compares theme configuration and current field setup and returns
   * an intersection of effectivly valid field names.
   */
  private static function getValidFields() {
    if (!isset(self::$validFields)) {
      $availableFields = array();
      // Load the theme configuration and order active fields by weight.
      if ($fields = theme_get_setting('source_fields', 'bilderblock')) {
        // Filter fields by "active" property.
        $fields = array_keys(array_filter((array) $fields, function($val) {
          return $val['active'] == 1;
        }));
        // Sort remaining fields by weight.
        uasort($fields, function($a, $b) {
          if ($a['weight'] > $b['weight']) {
            return 1;
          }
          elseif ($a['weight'] < $b['weight']) {
            return -1;
          }
          else {
            return 0;
          }
        });
        if (!empty($fields)) {
          if ($availableFields = array_keys(field_info_field_map())) {
            // Intersect available and configured fields.
            $availableFields = array_intersect($fields, $availableFields);
          }
        }
      }
      self::$validFields = $availableFields;
    }
    return self::$validFields;
  }

  /**
   * Provides the DRUPAL_ROOT relative path to a theme-related image source.
   *
   * Checks for the config state (default/custom) of any theme-configurable
   * image and returns a relative location for the (unstyled) resource.
   *
   * @param string $stub
   *   (Optional) the theme settings filename stub to process (e.g. "home_icon").
   *   Defaults to "logo". Intended to allow for looking up logo and home icon
   *   the same way.
   *
   * @return string
   *   The DRUPAL_ROOT relative path to the resource.
   */
  private static function resourceLocation($stub = 'logo') {
    $ret = '';
    // Convention rule for logo.
    $is_logo = $stub == 'logo';
    $images_path = $is_logo ? '' : 'images/';
    if (!$is_logo || theme_get_setting("toggle_${stub}", 'bilderblock')) {
      if (theme_get_setting("default_${stub}", 'bilderblock')) {
        $ret = drupal_get_path('theme', 'bilderblock') . "/${images_path}${stub}.png";
      }
      else {
        $ret = theme_get_setting("${stub}_path", 'bilderblock');
      }
    }
    return $ret;
  } // resourceLocation()

  /**
   * Retrieves the actual path to the dynamic CSS template.
   *
   * Builds the dynamic CSS path, checks whether the file is readable
   * and if so, returns the path.
   *
   * @return string|bool
   *   The template path, or FALSE, if the file is inaccessible.
   */
  private static function cssTemplatePath() {
    if ($themePath = drupal_get_path('theme', 'bilderblock')) {
      $includePath = "${themePath}/includes/dynamic_styles.inc";
      if (is_readable($includePath)) {
        return $includePath;
      }
    }
    return FALSE;
  }
}
