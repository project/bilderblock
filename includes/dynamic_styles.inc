<?php

/**
 * @file
 * Dynamic style template.
 * Will be parsed with actual theme settings into a cacheable css file.
 */

// Mini helper.
$tgs = function($s) {
  return theme_get_setting($s, 'bilderblock');
};

$logo = bilderblock::logoURL('logo');
$homeIcon = bilderblock::logoURL('home_icon');

$hover_state = $tgs('hover_enabled');
?>
/**
 * Dynamic bilderblock styles.
 */
body.bilderblock-logo div#menu-logo > a:before {
  content: url(<?php print $logo; ?>);
}

<?php if ($content_max_width = $tgs('content_max_width')): ?>
  /**
   * Set maximum content width to <?php print $content_max_width; ?> as configured,
   * but only in viewports with at least that size (overflow control).
   * @todo @refactor #BBM-182.
   */
  @media screen and (min-width: <?php print $content_max_width; ?>) {
    body > div#page > div#content-wrapper {
      max-width: <?php print $content_max_width; ?>;
    }
  }
<?php endif;

$hover_clear = $tgs('fx_hover_clear');

// Limit effects.
if ($hover_clear && $hover_state == bilderblock::HOVER_DESKTOP_NOTOUCH): ?>
  @media screen and (min-width: <?php print bilderblock::BREAKPOINT_DESKTOP_MIN; ?>px) {
<?php endif;

if ($hover_state || !$tgs('fx_hover_clear')) : ?>
  /** Effects limited to no-touch desktop devices. **/
  /* Overall filter animation speed. */
  div.slide-container,
  div#content-wrapper,
  div.teaser-sidestrip-inner,
  div.teaser-slide,
  div.teaser-slide img {
    -moz-transition-duration: <?php print $tgs('fx_speed'); ?>ms;
    -webkit-transition-duration: <?php print $tgs('fx_speed'); ?>ms;
    -o-transition-duration: <?php print $tgs('fx_speed'); ?>ms;
    transition-duration: <?php print $tgs('fx_speed'); ?>ms;
  }
  <?php $opacity = $tgs('fx_opacity');
  if ($opacity != 100) : ?>
    /* Opacity */
    html:not(.touchevents) body.bilderblock-fx-opacity.bilderblock-fx-hover-clear div.slide-container:not(:focus):not(:hover),
    html:not(.touchevents) body.bilderblock-fx-opacity:not(.bilderblock-fx-hover-clear) div.slide-container {
      opacity: <?php print $opacity / 100; ?>;
    }
  <?php endif;

  // Prepare multiple filters.
  // Since "filter" attributes mutually override, we have to prepare a chain.
  $classes = $filters = array();

  // 1. Desaturation:
  $saturation = $tgs('fx_saturation');
  if ($saturation != 100) {
    //2
    $classes[] = 'bilderblock-fx-saturation';
    $filters[] = "saturate({$saturation}%)";
  }
  // 2. Tint
  if ($tint = $tgs('fx_tint')) {
    // 2
    $classes[] = 'bilderblock-fx-tint';
    $filters[] = 'url(#bilderblock-filter-tint)';
  }

  if (
    ($classes = implode('.', $classes))
    &&
    ($filters = implode(' ', $filters))
  ) : ?>
    html:not(.touchevents) body.<?php print $classes; ?>.bilderblock-fx-hover-clear div.slide-container:not(:focus):not(:hover) div.teaser-slide img,
    html:not(.touchevents) body.<?php print $classes; ?>:not(.bilderblock-fx-hover-clear) div.teaser-slide img {
      -moz-filter: <?php print $filters; ?>;
      -o-filter: <?php print $filters; ?>;
      -webkit-filter: <?php print $filters; ?>;
      filter: <?php print $filters; ?>;
    }
    /* Override: Unpublished nodes */
    html:not(.touchevents) body.<?php print $classes; ?>.bilderblock-fx-hover-clear div.slide-container.node-unpublished:not(:focus):not(:hover) div.teaser-slide img,
    html:not(.touchevents) body.<?php print $classes; ?>:not(.bilderblock-fx-hover-clear) div.slide-container.node-unpublished div.teaser-slide img {
      -moz-filter: <?php print $filters; ?> invert(100%);
      -o-filter: <?php print $filters; ?> invert(100%);
      -webkit-filter: <?php print $filters; ?> invert(100%);
      filter: <?php print $filters; ?> invert(100%);
    }
  <?php endif;
endif; // 1
if ($hover_clear && $hover_state == bilderblock::HOVER_DESKTOP_NOTOUCH): ?>
  }
<?php endif;

if (!$tgs('default_home_icon') && $homeIcon) : ?>
body:not(.bilderblock-fontawesome):not(.bilderblock-logo) div#navbar-logo > a,
body.bilderblock-logo:not(.bilderblock-fontawesome):not(.bilderblock-menu-overlay) div#navbar-logo > a,
body.bilderblock-fontawesome:not(.bilderblock-logo) div#navbar-logo > a,
body.bilderblock-fontawesome.bilderblock-logo:not(.bilderblock-menu-overlay) div#navbar-logo > a {
  height: 35px;
}
body:not(.bilderblock-fontawesome):not(.bilderblock-logo) div#navbar-logo > a:before,
body.bilderblock-logo:not(.bilderblock-fontawesome):not(.bilderblock-menu-overlay) div#navbar-logo > a.fa-home:before,
body.bilderblock-fontawesome:not(.bilderblock-logo) div#navbar-logo > a:before,
body.bilderblock-fontawesome.bilderblock-logo:not(.bilderblock-menu-overlay) div#navbar-logo > a.fa-home:before {
  content: url(<?php print $homeIcon; ?>);
  height: 35px;
}
/* Reset standard height for custom logo. */
body.bilderblock-fontawesome div#navbar-logo a.bb-navbar {
  margin-top: 0;
}
<?php endif; ?>

@media screen and (orientation: landscape) and (min-width: 641px) {
  /** Fixed menu/flyout sidebar for "desktop" and above. **/
  /* Sidebar container abstracts menu part from outer parts. */
  div#sidebar-container,
  div#menu {
    width: <?php print $tgs('menu_expanded_width'); ?>;
  }
}

@media screen and (min-width: 641px) and (orientation: landscape) {
  /* Outer container (default state/menu opened) */
  html:not(.js) body > div#page > div#content-wrapper,
  html.js body:not(.bilderblock-menu-closed) > div#page > div#content-wrapper {
    /* Set default margin for acontent to configured open menu width. */
    margin-left: <?php print $tgs('menu_expanded_width'); ?>;
  }
}
