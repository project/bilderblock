<?php

require_once dirname(__FILE__) . '/includes/bilderblock.class.inc';

/**
 * @file
 * Template functions for bilderblock theme.
 */

/**
 * Implements hook_preprocess_html().
 */
function bilderblock_preprocess_html(&$vars) {

  // Add jQuery cookie.
  drupal_add_library('system', 'jquery.cookie', TRUE);
  // Set mobile-friendly viewport.
  drupal_add_html_head(array(
    '#type' => 'html_tag',
    '#tag' => 'meta',
    '#attributes' => array(
      'name' => 'viewport',
      'content' => 'width=device-width, initial-scale=1, user-scalable=yes',
    ),
  ), 'bilderblock_viewport');

  // Set global font class for body.
  $vars['classes_array'][] = 'bilderblock-font-global';

  // - Clear hover effects, if active.
  $hover_enabled = theme_get_setting('hover_enabled', 'bilderblock');
  if ($hover_clear = theme_get_setting('fx_hover_clear', 'bilderblock')) {
    $vars['classes_array'][] = 'bilderblock-fx-hover-clear';
  }
  if ($hover_enabled || !$hover_clear) {
    // - Saturation, unless 100% (=ineffective)
    if (theme_get_setting('fx_saturation', 'bilderblock') != 100) {
      $vars['classes_array'][] = 'bilderblock-fx-saturation';
    }
    // - Opacity, unless 100% (=ineffective)
    if (theme_get_setting('fx_opacity', 'bilderblock') < 100) {
      $vars['classes_array'][] = 'bilderblock-fx-opacity';
    }
    // - Tint, if any.
    if (theme_get_setting('fx_tint', 'bilderblock') > 0) {
      $vars['classes_array'][] = 'bilderblock-fx-tint';
    }
  }
  // Set CSS zoom to fallback, if configured.
  if (theme_get_setting('zoom_mode', 'bilderblock') == bilderblock::ZOOM_FALLBACK) {
    $vars['classes_array'][] = 'bilderblock-zoom-fallback';
  }
  if ($hover_enabled && theme_get_setting('menu_hover', 'bilderblock')) {
    $vars['classes_array'][] = 'bilderblock-menu-hover';
  }
  // Sidebar ("menu") logo/title/slogan display.
  if (theme_get_setting('toggle_logo', 'bilderblock')) {
    $vars['classes_array'][] = 'bilderblock-logo';
  }
  if (theme_get_setting('toggle_name', 'bilderblock')) {
    $vars['classes_array'][] = 'bilderblock-name';
  }
  if (theme_get_setting('toggle_slogan', 'bilderblock')) {
    $vars['classes_array'][] = 'bilderblock-slogan';
  }

  if (theme_get_setting('toggle_post_image_border', 'bilderblock')) {
    $vars['classes_array'][] = 'bilderblock-post-image-border';
  }

  if (module_exists('fontawesome')) {
    $vars['classes_array'][] = 'bilderblock-fontawesome';
  }

  // Evaluate fallback menu toggle links and current user menu state.
  // Determine the next menu state from theme settings or override
  // through the client cookie, if available and allowed.
  if (theme_get_setting('menu_cookie', 'bilderblock')) {
    $menu_closed =
        (
          empty($_COOKIE['Drupal_visitor_bilderblockMenu'])
          &&
          (bool) theme_get_setting('menu_default_closed', 'bilderblock')
        )
        ||
        $_COOKIE['Drupal_visitor_bilderblockMenu'] == 'closed'
    ;
  }
  else {
    $menu_closed = (bool) theme_get_setting('menu_default_closed', 'bilderblock');
  }
  // Set CSS classes according to updated menu state.
  if ($menu_closed) {
    $vars['classes_array'][] = 'bilderblock-menu-closed';
  }
}

/**
 * Implements hook_process_html().
 */
function bilderblock_process_html(&$vars) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_html_alter($vars);
  }
}

/**
 * Implements hook_preprocess_maintenance_page().
 */
function bilderblock_preprocess_maintenance_page(&$variables) {
  // By default, site_name is set to Drupal if no db connection is available
  // or during site installation. Setting site_name to an empty string makes
  // the site and update pages look cleaner.
  // @see template_preprocess_maintenance_page
  if (!$variables['db_is_active']) {
    $variables['site_name'] = '';
  }
  // Add default font attribute.
  $variables['classes_array'][] = 'bilderblock-font-global';
  drupal_add_css(drupal_get_path('theme', 'bilderblock') . '/css/maintenance-page.css', array(
    'weight' => 99,
    'group' => CSS_THEME,
  ));
}

/**
 * Checks if this result has been flagged as "teaser".
 *
 * @see bilderblock_preprocess_page().
 */
function bilderblock_preprocess_search_result(&$vars) {
  // In bilderblock_preprocess_page, we conditionally insert flags if we have
  // moved teasers to the "light box" (teaser node) area.
  // The following allows the template to suppress output.
  // @see search-result.tpl.php.
  $vars['is_teaser'] = !empty($vars['result']['is_teaser']);
}

/**
 * Checks if there are any results being rendered as teasers.
 *
 * Prevent "no results" section, if we have successfully taken out teasers.
 * This is safer than to simply check for $vars['search_result'] being empty;
 * also the results template should be empowered to distinguish between
 * "empty but teasers" and "no result".
 * $has_teasers can be evaluated in the template file.
 *
 * @see search-results.tpl.php
 * @see bilderblock_preprocess_search_result().
 */
function bilderblock_preprocess_search_results(&$vars) {
  foreach ((array) $vars['results'] as $item) {
    if (!empty($item['is_teaser'])) {
      $vars['has_teasers'] = TRUE;
      break;
    }
  }
}

/**
 * Re-orders page content (teasers) at theming level.
 *
 * Since the "light box"-style teaser area is theme-specific,
 * there is no Drupal way to re-order components in the page render array
 * other than to re-assemble the pre-themed render array from top to bottom.
 * This function primarily looks up whether the following (optional) components
 * exist in the system_main block (Drupal's hard-wired main content area):
 * - "nodes" (any node children in the system_main_block render array.
 * - "search_results" (result snippets from Drupal standard/core search)
 * If such elements are found, they are "moved" from their original location
 * to the 'teasers' variable, so they can be rendered in place (i. e. in the page
 * template).
 * Also note the companion workaround for blocks.
 * @see page.tpl.php
 * @see search_result.tpl.php
 * @see search_results.tpl.php
 * @see bilderblock_preprocess_block().
 */
function bilderblock_preprocess_page(&$vars) {

  // Get and apply global hover switch setting.
  $hover_state = theme_get_setting('hover_enabled', 'bilderblock');
  $theme_path = drupal_get_path('theme', 'bilderblock');

  $css_all = $css_fx = array(
    'group' => CSS_THEME,
    'media' => 'screen and (min-width: 641px) and (orientation: landscape)',
  );
  if ($hover_state == bilderblock::HOVER_ALWAYS) {
    unset($css_fx['media']);
  }

  if ($hover_state) {
    drupal_add_css("${theme_path}/css/hover.css", $css_fx);
  }
  // Add hover_fx CSS, if configured and allowed.
  if ($hover_state || !theme_get_setting('fx_hover_clear', 'bilderblock')) {
    drupal_add_css("$theme_path/css/hover_fx.css", $css_fx);
  }
  // Add menu_hover CSS, if configured.
  if ($hover_state && theme_get_setting('menu_hover', 'bilderblock')) {
    drupal_add_css("$theme_path/css/menu_hover.css", $css_all);
  }
  // Prepare inline CSS options.
  // Image sizes (retrieves prepared and cached style data from theme class registry.
  drupal_add_css(bilderblock::getImageSizeStyles(), array(
    'type' => 'inline',
    'group' => CSS_THEME,
    'media' => sprintf('screen and (min-width: %spx) and (orientation: landscape)', bilderblock::BREAKPOINT_DESKTOP_MIN),
  ));
  // Call dynamic CSS from cache.
  $dynamic_css = bilderblock::css();
  // Add variables for site_name and site_slogan.
  $dynamic_css .= format_string("div#menu-title div.name > a:before {content: '!site_name';} div#menu-title div.slogan:before {content: '!site_slogan';}", array(
    '!site_name' => $vars['site_name'],
    '!site_slogan' => $vars['site_slogan'],
  ));
  drupal_add_css($dynamic_css, array(
    'type' => 'inline',
    'group' => CSS_THEME,
  ));

  // JS menu state tracking.
  drupal_add_js(array(
    'bilderblock' => array(
      'menuCookie' => theme_get_setting('menu_cookie', 'bilderblock'),
    ),
  ), array(
    'type' => 'setting',
    'group' => JS_THEME,
  ));

  // Pass zoom mode to template.
  $vars['zoom_mode'] = theme_get_setting('zoom_mode', 'bilderblock');

  // Prepare svg "tint" filter matrix, if configured.
  $vars['tint_matrix'] = bilderblock::colorMatrix('film_strip_text');

  // Prepare teaser container.
  $teasers = array();
  if (!isset($vars['teasers'])) {
    $vars['teasers'] = array();
  }
  $teasers += $vars['teasers'];

  // Extract all teaser nodes from the original array.
  if (
    isset($vars['page']['content']['system_main']['nodes'])
    &&
    is_array($vars['page']['content']['system_main']['nodes'])
    &&
    !empty($vars['page']['content']['system_main']['nodes'])
  ) {
    foreach (element_children($vars['page']['content']['system_main']['nodes']) as $nid) {
      if (
        isset($vars['page']['content']['system_main']['nodes'][$nid]['#view_mode'])
        &&
        $vars['page']['content']['system_main']['nodes'][$nid]['#view_mode'] == 'teaser'
      ) {
        $teasers[$nid] = $vars['page']['content']['system_main']['nodes'][$nid];
        unset($vars['page']['content']['system_main']['nodes'][$nid]);
      }
    }
    // Clean up nodes container, if empty.
    if (!element_children($vars['page']['content']['system_main']['nodes'])) {
      unset($vars['page']['content']['system_main']['nodes']);
    }

    // Remove term heading if there is no content.
    // In theming, there is no (known to me) option to see if the term is really
    // free from any fields without adding much overhead.
    // This workaround thus checks the complete term.
    if ($test =@ $vars['page']['content']['system_main']['term_heading']['term']) {
      // theme_taxonomy_term will always add '#prefix' and '#suffix'.
      // Thus, we will only theme all child fields.
      unset($test['#theme']);
      if (!$test = render($test)) {
        // Unless this logic is flawed, the section is empty. Drop it.
        unset($vars['page']['content']['system_main']['term_heading']);
      }
    }
    // Finally remove system main block, if there are no other children.
    // @todo Should this happen after the search_results_path?
    if (!element_children($vars['page']['content']['system_main'])) {
      unset($vars['page']['content']['system_main']);
    }
  }
  $vars['teasers'] += $teasers;

  // Transform search results into teaser nodes, too.
  $teasers = array();
  $results =& $vars['page']['content']['system_main']['search_results']['#results'];

  // Not all modules deliver nodes.
  if (!$result_type =@ $vars['page']['content']['system_main']['search_results']['#module']) {
    // Make sure we are on a result page at all.
    $result_type = FALSE;
  }

  if (
    // Only process certain result types as teasers.
    !empty($result_type)
    &&
    in_array($result_type, array('node'))
    &&
    isset($results)
    &&
    is_array($results)
    &&
    !empty($results)
  ) {
    foreach (element_children($results) as $nid) {
      if (
        (isset($results[$nid]['node']))
        &&
        ($teaser = node_view($results[$nid]['node'], 'teaser'))
      ) {
        $teasers[$nid] = $teaser;
        $results[$nid]['is_teaser'] = 1;
      }
    }
    $vars['teasers'] += $teasers;
  }
  else {
    // For any reasons, this does not happen with search results.
    // It seems theme specific, yet there is no explanation.
    pager_default_initialize(0,1);
  }
  // Remove empty search area completely if teasers were found
  // (to prevent the "yielded no results display).
  if (empty($results) && !empty($vars['teasers'])) {
    unset($vars['page']['content']['system_main']['search_results']);
  }

  // Re-create a pager at the absolute page bottom.
  $vars['pager'] = theme('pager', array('tags' => NULL));
  // Remove the original one, for we will always page at the page bottom.
  unset($vars['page']['content']['system_main']['pager']);

  // Set exposed image ("slide").
  if (!theme_get_setting('disable_exposed', 'bilderblock')) {
    $slide = array();
    // 1. For standard nodes
    if (
      (!empty($vars['node']))
      &&
      ($nodes =@ $vars['page']['content']['system_main']['nodes'])
      &&
      ($nid = element_children($nodes))
    ) {
      if ($nid = array_shift($nid)) {
        if ($fieldname = bilderblock::imageSource($vars['node'])) {
          if ($slide =@ $vars['page']['content']['system_main']['nodes'][$nid][$fieldname]) {
            $vars['slide'] = $slide;
            unset($vars['page']['content']['system_main']['nodes'][$nid][$fieldname]);
          }
        }
      }
    }
    // 2. For comment_node displays.
    elseif (
      ($comment_node =@ $vars['page']['content']['system_main']['comment_node'])
      &&
      ($node =@ $comment_node['#node'])
      &&
      (is_object($node))
    ) {
      if ($fieldname = bilderblock::imageSource($node)) {
        if ($slide =@ $comment_node[$fieldname]) {
          $vars['slide'] = $slide;
          unset($vars['page']['content']['system_main']['comment_node'][$fieldname]);
        }
      }
    }
  }
  // Remove empty system_main block completely.
  $main_blocks = element_children($vars['page']['content']['system_main']);
  if (empty($main_blocks)) {
    unset($vars['page']['content']['system_main']);
  }
}

/**
 * Override or insert variables into the page template.
 */
function bilderblock_process_page(&$vars) {
  // Set toggle_breadcrumb var.
  if (!theme_get_setting('toggle_breadcrumb', 'bilderblock')) {
    $vars['breadcrumb'] = '';
  }
}

function bilderblock_preprocess_block(&$vars) {
  // Workaround. Motivation:
  // Determine whether inside a block, any teaser-slide is present.
  // Problem: $content can be anything, no render array is mandatory
  // (hard-wired HTML may be the case). This also applies to views content.

  // The below comment is inserted by the theme itself, in the node template.
  // @see node.tpl.php
  //
  // @deprecated @todo @review #BBM-277
  if (preg_match(
    '#<!--bilderblock-is-teaser-->#',
    $vars['content'],
    $m
  )) {
    $vars['classes_array'][] = 'bilderblock-has-teasers';
  }
}

/**
 * Override or insert variables into the node template.
 */
function bilderblock_preprocess_node(&$vars) {

  if (
    !empty($vars['content'])
    &&
    !empty($vars['content']['links'])
    &&
    !empty($vars['content']['links']['comment'])
    ) {
    // Provide a single comments link for the teaser view.
    // This link is either the "n comments" or the "add comment"
    // link.
    if ($v =@ $vars['content']['links']['comment']['#links']) {
      if (
        // Either there are already comments
        ($l =@ $v['comment-comments'])
        ||
        ($l =@ $v['comment-add'])
      ) {
        $vars['comments_link'] = array(
          '#theme' => 'link',
          '#text' => $l['title'],
          '#path' => $l['href'],
          '#options' => array(
            'attributes' => $l['attributes'],
            'fragment' => $l['fragment'],
            'html' => TRUE,
          ),
        );
      }
    }
  }

  $vars['labels'] = bilderblock::getLabels();

  // Provide suitable fallback image in any case.
  $fallback_image = bilderblock::getFallbackImage($vars['node'], $class);
  // Hard-wired markup is the closest way to preserve any alreay altered URL.
  // Also, the path has already been validated.
  $vars['fallback'] = sprintf('<a href="%s">%s</a>', $vars['node_url'], $fallback_image);
  // Try to prepare a teaser slide:
  if ($fieldname =@ bilderblock::imageSource($vars['node'])) {
    if ($slide =@ $vars['content'][$fieldname]) {
      $vars['slide'] = $slide;
      $class = bilderblock::getImageSizeClass($slide);
    }
  }
  $vars['slide_class'] = $class;

  // Re-format date.
  $vars['date'] = format_date($vars['created'], 'short');
}

/**
 * Implements hook_theme_registry_alter().
 */
function bilderblock_theme_registry_alter() {
  // This hook is the most (and only) sensible trigger to check if
  // the theme has been updated and to then rebuild the dynamic parts.
  if (bilderblock::cssUpdated()) {
    bilderblock::css(TRUE);
  }
}
