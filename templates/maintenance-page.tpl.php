<?php

/**
 * @file
 * Maintenance page to be displayed when the site is offline.
 *
 * @see template_preprocess()
 * @see template_preprocess_maintenance_page()
 */

$home_label = t('Home');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
  "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language->language; ?>" lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>">
  <head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <?php print $styles; ?>
    <?php print $scripts; ?>
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes;?>><?

/* ------------------ inner ----------------------- */

?>
    <div id="page">
      <div id="content-wrapper">
        <div class="jump-spacer"><div id="jump-content" class="jump-target"></div></div>
        <div id="content">
          <?php if ($logo || $site_name || $site_slogan) : ?>
          <div class="menu-wrapper region">
            <div id="menu-header" class="block clearfix">
              <div id="menu-title">
                <?php if ($logo): ?>
                <div id="menu-logo">
                    <a href="<?php print $front_page; ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print $site_slogan; ?>" title="<?php print $home_label; ?>" /></a>
                </div><!--/#logo-->
                <?php endif;
                if ($site_name): ?>
                <div id="site-name" class="name">
                  <strong>
                    <a href="<?php print $front_page; ?>" title="<?php print $home_label; ?>" rel="home"><span><?php print $site_name; ?></span></a>
                  </strong>
                </div><!--/#site-name-->
                <?php endif;

                if ($site_slogan): ?>
                <div id="site-slogan" class="slogan">
                  <?php print $site_slogan; ?>
                </div>
                <?php endif; ?>
              </div><!--#menu-title-->
            </div><!--/#menu-header.block-->
          </div><!--/.menu-wrapper.region-->
          <?php endif;

          if ($preface): ?>
          <hr class="element-invisible" />
          <div id="preface">
            <?php print $preface; ?>
          </div><!--/#preface-->
          <?php endif;

          if ($title): ?>
          <div><!--Helps separating inline-blocks, which are used only for the background color.-->
            <div id="title-wrapper">
              <h1 class="title" id="page-title">
                <?php print $title; ?>
              </h1>
            </div><!--/#title-wrapper-->
          </div>
          <?php endif; // $title

          if ($messages): ?>
          <div id="messages"><div class="section clearfix">
            <?php print $messages; ?>
          </div></div> <!-- /.section, /#messages -->
          <?php endif; // $messages

          if ($help) : ?>
          <hr class="element-invisible" />
          <?php print $help;
          endif; // $help

          if ($content_top): ?>
            <hr class="element-invisible" />
            <div id="content-top">
              <?php print $content_top; ?>
            </div><!--/#content-top-->
          <?php endif;

          // Prepare exposed image.
          if ($exposed) :
            print bilderblock::markupExposed($exposed, $zoom_mode);
          endif;
          if ($slide = render($slide)) :
            print bilderblock::markupExposed($slide, $zoom_mode);
          endif;

          if ($teasers): ?>
          <div id="teaser-slides" class="bilderblock-has-teasers clearfix">
            <?php print $teasers; ?>
          </div>
          <?php endif; // $teasers

          if ($tabs = render($tabs)) : ?>
          <div class="tabs">
            <?php print $tabs; ?>
          </div>
          <?php endif;

          if ($content) : ?>
          <div class="content-wrapper region">
            <div class="block">
              <?php print $content; ?>
            </div><!--/.block-->

          <?php endif; ?>
          </div><!--/.content-wrapper-->
        </div><!--/#content-->

      </div><!--/#content-wrapper-->
      <? print render($title_suffix); ?>
    </div><!-- /#page -->
<?

/* ---------------- end inner --------------------- */

?>
  </body>
</html>
