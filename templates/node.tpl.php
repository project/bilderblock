<?php

/**
 * @file
 * Shared node-teaser include.
 * Used for standard node display, and also for search result display.
 *
 * @see node.tpl.php
 * @see search_result.tpl.php
 *
 * Available variables:
 * - $title: the (sanitized) title of the node.
 * - $content: An array of node items. Use render($content) to print them all,
 *   or print a subset such as render($content['field_example']). Use
 *   hide($content['field_example']) to temporarily suppress the printing of a
 *   given element.
 * - $user_picture: The node author's picture from user-picture.tpl.php.
 * - $date: Formatted creation date. Preprocess functions can reformat it by
 *   calling format_date() with the desired parameters on the $created variable.
 * - $name: Themed username of node author output from theme_username().
 * - $node_url: Direct URL of the current node.
 * - $display_submitted: Whether submission information should be displayed.
 * - $submitted: Submission information created from $name and $date during
 *   template_preprocess_node().
 * - $classes: String of classes that can be used to style contextually through
 *   CSS. It can be manipulated through the variable $classes_array from
 *   preprocess functions. The default values can be one or more of the
 *   following:
 *   - node: The current template type; for example, "theming hook".
 *   - node-[type]: The current node type. For example, if the node is a
 *     "Blog entry" it would result in "node-blog". Note that the machine
 *     name will often be in a short form of the human readable label.
 *   - node-teaser: Nodes in teaser form.
 *   - node-preview: Nodes in preview mode.
 *   The following are controlled through the node publishing options.
 *   - node-promoted: Nodes promoted to the front page.
 *   - node-sticky: Nodes ordered above other non-sticky nodes in teaser
 *     listings.
 *   - node-unpublished: Unpublished nodes visible only to administrators.
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 *
 * Other variables:
 * - $node: Full node object. Contains data that may not be safe.
 * - $type: Node type; for example, story, page, blog, etc.
 * - $comment_count: Number of comments attached to the node.
 * - $uid: User ID of the node author.
 * - $created: Time the node was published formatted in Unix timestamp.
 * - $classes_array: Array of html class attribute values. It is flattened
 *   into a string within the variable $classes.
 * - $zebra: Outputs either "even" or "odd". Useful for zebra striping in
 *   teaser listings.
 * - $id: Position of the node. Increments each time it's output.
 *
 * Node status variables:
 * - $view_mode: View mode; for example, "full", "teaser".
 * - $teaser: Flag for the teaser state (shortcut for $view_mode == 'teaser').
 * - $page: Flag for the full page state.
 * - $promote: Flag for front page promotion state.
 * - $sticky: Flags for sticky post setting.
 * - $status: Flag for published status.
 * - $comment: State of comment settings for the node.
 * - $readmore: Flags true if the teaser content of the node cannot hold the
 *   main body content.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * Field variables: for each field instance attached to the node a corresponding
 * variable is defined; for example, $node->body becomes $body. When needing to
 * access a field's raw values, developers/themers are strongly encouraged to
 * use these variables. Otherwise they will have to explicitly specify the
 * desired field language; for example, $node->body['en'], thus overriding any
 * language negotiation rule that was previously applied.
 *
 * Bilderblock theme variables:
 * - $comments_link: Render array to either existing comments or comment form.
 * - $fallback: Rendered markup for a fallback image styled according to this
 *              node/field type.
 * - $slide: Render array for the slide or exposed image.
 * - $slide_class: Class to map the teaser slide dimensions.
 * - $labels: Keyed array of translated strings:
 *   - date: Label for the date field (unformatted). May be empty.
 *   - by: Label for the "submitted" meta text.
 *
 * @see template_preprocess()
 * @see template_preprocess_node()
 * @see template_process()
 */

if ($view_mode == 'teaser') {
  $classes .= " bilderblock-font-slide slide-container clearfix";
}

?>
<div id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
<?php if ($view_mode == 'teaser'): ?><hr class="element-invisible" />
<!--bilderblock-is-teaser-->
  <div class="teaser-sidestrip teaser-header">
    <div class="teaser-sidestrip-wrapper">
      <div class="teaser-sidestrip-inner">
        <div class="teaser-label teaser-title">
          <h2><a href="<?php print $node_url; ?>" title="<?php print t('Show post'); ?>" rel="bookmark"><?php print $title; ?></a></h2>
        </div>
        <div class="teaser-label teaser-author">
          <?php if (!empty($labels['by'])): ?>
            <label><?php print $labels['by']; ?></label>
          <?php endif; ?>
          <?php print $name; ?>
        </div>
        <?php if (!empty($comments_link)): ?>
        <div class="teaser-label teaser-comments">
          <?php print render($comments_link); ?>
        </div>
        <?php endif; ?>
      </div><!--/.teaser-sidestrip-inner-->
    </div><!--/.teaser-sidestrip-wrapper-->
  </div><!--/.teaser-header-->
  <?php print render($title_prefix); ?>
  <div class="teaser-slide <?php print $slide_class; ?>">
  <?php
    // Slide/exposed image is always the first image of the field
    // named by $slide.
    if (!empty($slide)):
      print render($slide);
    else:
      // Fallback image.
      print $fallback;
    endif;
  ?></div><!--/.teaser-slide-->
  <?php print render($title_suffix); ?>
  <div class="teaser-sidestrip teaser-footer">
    <div class="teaser-sidestrip-wrapper">
      <div class="teaser-sidestrip-inner"><?php
      if (!empty($date)): ?>
      <div class="teaser-label teaser-date">
        <?php if (!empty($labels['date'])): ?>
          <label><?php print $labels['date']; ?></label>
        <?php endif; ?>
        <?php print $date; ?>
      </div><?php
      endif;
      // Find all term fields and render them in the footer stripe.
      foreach ($content as $key => $item) :
        if (!isset($item['#field_type']) || $item['#field_type'] != 'taxonomy_term_reference') :
          hide($content[$key]);
        endif;
      endforeach;
      // Remove any whitespace. The inner div must be empty
      // to trigger the :empty CSS pseudo class.
      print trim(render($content));
      ?></div><!--/.teaser-sidestrip-inner-->
    </div><!--/.teaser-sidestrip-wrapper-->
  </div>
<?php

/* End teaser view. */
else :
/* Default view. */
?>
  <div id="single-post-infos">
    <div id="single-post-infos-inner">
      <?php if (!empty($content['body'])): ?>
      <div id="content-text">
          <?php print render($content['body']); ?>
      </div><!--/#content-text-->
      <?php endif; ?>
      <div id="post-informational">
        <?php
          // Remove the "Add new comment" link on the teaser page or if the comment
          // form is being displayed on the same page.
          if ($teaser || !empty($content['comments']['comment_form'])) {
            unset($content['links']['comment']['#links']['comment-add']);
          }
          hide($content['comments']);
          hide($content['links']);
          print render($content);
        ?>
      </div><!--/#post-informational-->
      <?php if ($display_submitted): ?>
        <div class="meta submitted">
          <?php print $user_picture; ?>
          <?php print $submitted; ?>
        </div>
      <?php endif;
      // Only display the wrapper div if there are links.
      $links = render($content['links']);
      if ($links): ?>
      <div class="link-wrapper">
        <?php print $links; ?>
      </div>
      <?php endif;
      print render($content['comments']); ?>
    </div><!--#single-post-infos-inner-->
  </div><!--/#single-post-infos.image-post-->
<?php endif; /* End default view. */ ?>
</div><!--/.node-->
