<?php

/**
 * @file
 * Bilderblock general page template.
 *
 * Available variables:
 *
 * General utility variables:
 * - $base_path: The base URL path of the Drupal installation. At the very
 *   least, this will always default to /.
 * - $directory: The directory the template is located in, e.g. modules/system
 *   or themes/bartik.
 * - $is_front: TRUE if the current page is the front page.
 * - $logged_in: TRUE if the user is registered and signed in.
 * - $is_admin: TRUE if the user has permission to access administration pages.
 *
 * Site identity:
 * - $front_page: The URL of the front page. Use this instead of $base_path,
 *   when linking to the front page. This includes the language domain or
 *   prefix.
 * - $logo: The path to the logo image, as defined in theme configuration.
 * - $site_name: The name of the site, empty when display has been disabled
 *   in theme settings.
 * - $site_slogan: The slogan of the site, empty when display has been disabled
 *   in theme settings.
 * - $hide_site_name: TRUE if the site name has been toggled off on the theme
 *   settings page. If hidden, the "element-invisible" class is added to make
 *   the site name visually hidden, but still accessible.
 * - $hide_site_slogan: TRUE if the site slogan has been toggled off on the
 *   theme settings page. If hidden, the "element-invisible" class is added to
 *   make the site slogan visually hidden, but still accessible.
 *
 * Navigation:
 * - $main_menu (array): An array containing the Main menu links for the
 *   site, if they have been configured.
 * - $secondary_menu (array): An array containing the Secondary menu links for
 *   the site, if they have been configured.
 * - $breadcrumb: The breadcrumb trail for the current page.
 *
 * Page content (in order of occurrence in the default page.tpl.php):
 * - $title_prefix (array): An array containing additional output populated by
 *   modules, intended to be displayed in front of the main title tag that
 *   appears in the template.
 * - $title: The page title, for use in the actual HTML content.
 * - $title_suffix (array): An array containing additional output populated by
 *   modules, intended to be displayed after the main title tag that appears in
 *   the template.
 * - $messages: HTML for status and error messages. Should be displayed
 *   prominently.
 * - $tabs (array): Tabs linking to any sub-pages beneath the current page
 *   (e.g., the view and edit tabs when displaying a node).
 * - $action_links (array): Actions local to the page, such as 'Add menu' on the
 *   menu administration interface.
 * - $feed_icons: A string of all feed icons for the current page.
 * - $node: The node object, if there is an automatically-loaded node
 *   associated with the page, and the node ID is the second argument
 *   in the page's path (e.g. node/12345 and node/12345/revisions, but not
 *   comment/reply/12345).
 *
 * Regions:
 * - $page['preface']: Exposed region, e. g. for messages.
 * - $page['content_top']: Standard content, prior to priorized sections.
 * - $page['exposed']: Standard content added to the "exposed image" area.
 * - $page['teasers']: Content to render in the same area as teasers.
 * - $page['content']: Any standard node content.
 * - $page['sidebar']: Items for the sidebar.
 *
 * bilderblock theme vars:
 * - $slide: Render array for the exposed image on node pages
 *           (set by the theme; user the "exposed" region for standard content).
 * - $teasers: Render array for a section with all node teasers from this page.
 * - $pager: The pager render array from the system main block.
 * - $tint_matrix: If configured, an svg feColorMatrix string. Otherwise FALSE.
 * - $zoom_mode: One of the constants defining the site's CSS image zoom setting.
 *               @see template.php
 *
 * @see template_preprocess()
 * @see template_preprocess_page()
 * @see template_process()
 * @see bilderblock_process_html()
 * @see bilderblock_process_page()
 * @see html.tpl.php
 */

$home_label = t('Home');
$jump_menu_label = t('Show menu');
$up_label = t('Jump to content top');

?>
    <div id="page">
      <?php print render($title_prefix); ?>
      <div id="content-wrapper">
        <div class="jump-spacer"><div id="jump-content" class="jump-target"></div></div>
        <div id="content">
          <?php if (!empty($logo)): ?>
          <div id="logo" class="bb-print element-invisible">
              <a href="<?php print $front_page; ?>" rel="home"><img src="<?php print $logo; ?>" alt="<?php print $site_slogan; ?>" title="<?php print $home_label; ?>" /></a>
          </div><!--/#logo-->
          <?php endif;

          if ($site_name):
            if (!$is_front && $title): ?>
              <div id="site-name" class="bb-print element-invisible">
                <strong>
                  <a href="<?php print $front_page; ?>" title="<?php print $home_label; ?>" rel="home"><span><?php print $site_name; ?></span></a>
                </strong>
              </div><!--/#site-name-->
            <?php else: /* Use h1 when the content title is empty */ ?>
              <h1 id="site-name" class="bb-print element-invisible">
                <a href="<?php print $front_page; ?>" title="<?php print $home_label; ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </h1>
            <?php endif;
          endif;

          if ($site_slogan): ?>
            <div id="site-slogan" class="bb-print element-invisible">
              <?php print $site_slogan; ?>
            </div>
          <?php endif;

          if (!empty($logo) || !empty($site_name) || !empty ($site_slogan) || !empty($page['sidebar'])) : ?>
          <hr id="header-separator" class="bb-print element-invisible" />
          <?php endif;
          if (!empty($page['sidebar'])): ?>
          <div class="element-invisible"><small><a href="#menu"><?php print $jump_menu_label; ?></a></small></div>
          <?php endif;

          if ($preface = render($page['preface'])): ?>
            <hr class="element-invisible" />
            <div id="preface">
              <?php print $preface; ?>
            </div><!--/#preface-->
          <?php endif;

          if ($breadcrumb || (!$is_front && $title)): ?>
            <div><!--Helps separating inline-blocks, which are used only for the background color.-->
              <div id="title-wrapper">
              <?php if ($breadcrumb): ?>
                <div id="breadcrumb"><?php print $breadcrumb; ?></div>
              <?php endif;

              if (!$is_front && $title): ?>
                <h1 class="title" id="page-title">
                  <?php print $title; ?>
                </h1>
              <?php endif; ?>
              </div><!--/#title-wrapper-->
            </div>
          <?php endif; // $title_wrapper

          if ($messages): ?>
            <div id="messages"><div class="section clearfix">
              <?php print $messages; ?>
            </div></div> <!-- /.section, /#messages -->
          <?php endif; // $messages

          if ($help = render($page['help'])) : ?>
          <hr class="element-invisible" />
          <?php print $help;
          endif; // $help

          if ($action_links = render($action_links)): ?>
          <div><!--Helps separating inline-blocks, which are used only for the background color.-->
            <div id="action-links-wrapper">
              <ul class="action-links">
                <?php print $action_links; ?>
              </ul>
            </div><!--/#action-links-wrapper-->
          </div>
          <?php endif; // $action_links

          if ($content_top = render($page['content_top'])): ?>
            <hr class="element-invisible" />
            <div id="content-top">
              <?php print $content_top; ?>
            </div><!--/#content-top-->
          <?php endif;

          // Prepare exposed image.
          if ($exposed = render($page['exposed'])) :
            print bilderblock::markupExposed($exposed, $zoom_mode);
          endif;
          if ($slide = render($slide)) :
            print bilderblock::markupExposed($slide, $zoom_mode);
          endif;

          $teaser_region = render($page['teasers']);
          if (($teasers = render($teasers)) || (!empty($teaser_region))): ?>
          <div id="teaser-slides" class="bilderblock-has-teasers clearfix">
            <?php print $teasers;
            print $teaser_region; ?>
          </div>
          <?php endif; // $teasers

          if ($tabs = render($tabs)) : ?>
            <div class="tabs">
              <?php print $tabs; ?>
            </div>
          <?php endif;

          if ($content = render($page['content'])) :
            print $content;
          endif;

          if ($pager = render($pager)) : ?>
            <div class="region">
              <div class="block">
                <?php print $pager; ?>
              </div><!--/.block-->
            </div><!--/.region-->
          <?php endif; ?>

        </div><!--/#content-->

      </div><!--/#content-wrapper-->
      <? print render($title_suffix); ?>

      <hr class="element-invisible" />

      <div id="nav-wrapper">
        <div id="navbar">
          <div id="navbar-logo" class="navbar-section"><a class="fa fa-home bb-navbar" href="<?php print $front_page; ?>" rel="home" title="<?php print $home_label; ?>"></a></div>
          <div id="navbar-switches" class="navbar-section">
            <div id="navbar-jump-content" class="navbar-switch">
              <a class="fa fa-arrow-circle-up bb-navbar" id="bb-top" href="#jump-content" title="<?php print $up_label; ?>"></a>
            </div><!--/#navbar-jump-content-->
            <div id="navbar-toggle" class="navbar-switch">
              <a class="fa fa-navicon bb-navbar" id="bb-toggle" href="#jump-menu" title="<?php print $jump_menu_label; ?>"></a>
            </div><!--/#navbar-toggle-->
          </div><!--/#navbar-switches-->
        </div><!--/#navbar-->
        <div id="menu">
          <div class="jump-spacer"><div id="jump-menu" class="jump-target"></div></div>
          <div id="sidebar-container">
            <div id="sidebar">
              <div id="menu-header" class="clearfix">
                <div id="menu-title">
                  <div id="menu-logo"<?php if (!empty($logo)): ?> class="has-logo"<?php endif; ?>><a href="<?php print $front_page; ?>" rel="home" title="<?php print $home_label; ?>"></a></div>
                  <?php if ($site_name): ?>
                  <div class="name"><a href="<?php print $front_page; ?>" rel="home" title="<?php print $home_label; ?>"></a></div>
                  <?php endif;
                  if ($site_slogan): ?>
                  <div class="slogan"></div>
                  <?php endif; ?>
                </div><!--/#menu-title-->
                <div id="menu-toggle-small" class="menu-small"><a title="<?php print $jump_menu_label; ?>" href="#jump-menu"></a></div>
              </div><!--/#menu-header-->
              <div id="menu-inner">
                <?php if ($main_menu): ?>
                <div id="main-menu" class="navigation">
                  <?php print theme('links__system_main_menu', array(
                    'links' => $main_menu,
                    'attributes' => array(
                      'id' => 'main-menu-links',
                      'class' => array('links', 'clearfix'),
                    ),
                    'heading' => array(
                      'text' => t('Main menu'),
                      'level' => 'h2',
                    ),
                  )); ?>
                </div> <!-- /#main-menu -->
                <?php endif;
                print render($page['sidebar']);
                if ($secondary_menu): ?>
                <div id="secondary-menu" class="navigation">
                  <?php print theme('links__system_secondary_menu', array(
                    'links' => $secondary_menu,
                    'attributes' => array(
                      'id' => 'secondary-menu-links',
                      'class' => array('links', 'clearfix'),
                    ),
                    'heading' => array(
                      'text' => t('Secondary menu'),
                      'level' => 'h2',
                    ),
                  )); ?>
                </div> <!-- /#secondary-menu -->
                <?php endif; ?>
              </div><!--/#menu-inner-->
            </div><!--/#sidebar-->
          </div><!--/#sidebar-container-->
        </div><!--/#menu-->
      </div><!--/#nav-wrapper-->
    </div><!-- /#page -->
    <?php
      // Custom svg color filter, if configured.
      if ($tint_matrix):
    ?><svg>
      <filter id="bilderblock-filter-tint">
        <feColorMatrix type="matrix" values="<?php print $tint_matrix; ?>" />
      </filter>
    </svg>
    <?php endif; ?>
