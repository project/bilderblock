<?php

require_once drupal_get_path('theme', 'bilderblock') . '/template.php';

/**
 * @file
 * Advanced bilderblock theme settings.
 */

/**
 * Extend theme settings form with bilderblock specific items.
 */
function bilderblock_form_system_theme_settings_alter(&$form, $form_state) {

  $tgs = function($k) {
    return theme_get_setting($k, 'bilderblock');
  };

  // Vertical tabs style fix helper.
  // @see https://www.drupal.org/node/1143712
  $stylefix = array(
    'style' => 'width: 100%; max-width: 100%; width: auto;',
  );


  // Place vertical tabs directly after the preview area.
  $form['theme_settings_tabs'] = array(
    '#type' => 'vertical_tabs',
    '#weight' => -1,
  );

  // Move existing theme settings into tabs, too.
  $form['theme_settings']['#group'] = 'theme_settings_tabs';

  // Add "toggle breadcrumb" setting.
  $form['theme_settings']['toggle_breadcrumb'] = array(
    '#type' => 'checkbox',
    '#title' => t('Breadcrumb'),
    '#default_value' => $tgs('toggle_breadcrumb'),
  );

  // Check layout config status and prepare options hint.
  $layout_hint = '';
  $layout_type = $tgs('layout_type');
  if (FALSE === $layout_type) {
    // FALSE is the settings default.
    $layout_hint = format_string(' <strong>!hint</strong>', array(
      '!hint' => t('You are currently using this setting. If you like it, select this option to make the warning message disappear.'),
    ));
  }

  // Add layout settings.
  $hover_never = t('No hover effects, never');
  $hover_desktop = t('Hover on desktop');
  $hover_always = t('Hover effects on every device');
  $form['theme_settings']['layout'] = array(
    '#weight' => 10,
    '#type' => 'fieldset',
    '#title' => t('General display'),
    '#description' => t('This section contains options for the general page look & feel.'),
    '#collapsible' => TRUE,
    '#group' => 'theme_settings_tabs',
    'hover_enabled' => array(
      '#type' => 'select',
      '#title' => t('Hover effects'),
      '#description' => t('Hover effects are usually useless on touch devices and may degrade site usability. On Javascript-enabled clients, you can install "modernizr" module to detect hover capability (the theme will recognize the modernizr "touchevents" class), however, this is limited to Javascript-enabled clients. Configure whether and when hover effects are available, and by which conditions. Also note the corresponding "clear on hover" setting for teaser slide effects.<ul><li>Use %never to suppress all hover effects (regardless of subsequent settings).</li><li>Select %desktop to disable hover effects for all none-desktop devices. This is some kind of compromise which assumes that touch-devices will usually support Javascript (you should consider modernizr).</li><li>Use %always, if you accept the degradation for users without mouse and without Javascript (you _really_ should install modernizr).</li></ul>', array(
        '%never' => $hover_never,
        '%desktop' => $hover_desktop,
        '%always' => $hover_always,
      )),
      '#options' => array(
        bilderblock::HOVER_NEVER => $hover_never,
        bilderblock::HOVER_DESKTOP_NOTOUCH => $hover_desktop,
        bilderblock::HOVER_ALWAYS => $hover_always,
      ),
      '#default_value' => $tgs('hover_enabled'),
    ),
    'fx_speed' => array(
      '#type' => 'textfield',
      '#size' => 5,
      '#field_suffix' => t('ms', array(), array('context' => 'milliseconds')),
      '#title' => t('Effects duration'),
      '#description' => t('Enter an integer value of zero or more milliseconds.'),
      '#default_value' => $tgs('fx_speed'),
      '#attributes' => $stylefix,
    ),
    'content_dimensions' => array(
      '#type' => 'fieldset',
      '#title' => t('Content'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'content_max_width' => array(
        '#type' => 'textfield',
        '#size' => 5,
        '#title' => t('Content width'),
        '#description' => t('Optionally limit the actual content width by a valid CSS value. Should correspond to your menu width value.'),
        '#field_prefix' => 'max-width: ',
        '#field_suffix' => ';',
        '#default_value' => $tgs('content_max_width'),
        '#attributes' => $stylefix,
      ),
    ),
    'menu_dimensions' => array(
      '#type' => 'fieldset',
      '#title' => t('Menu'),
      '#description' => t('Configure menu display options, like hover behavior, dimensions and presets.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'menu_expanded_width' => array(
        '#type' => 'textfield',
        '#size' => 5,
        '#title' => t('Expanded width'),
        '#description' => t('Overall maximum sidebar size when expanded, may be any valid CSS width value.'),
        '#default_value' => $tgs('menu_expanded_width'),
        '#attributes' => $stylefix,
      ),
      'menu_default_closed' => array(
        '#type' => 'checkbox',
        '#title' => t('Collapse menu by default'),
        '#description' => t('In "desktop" mode and landscape orientation and with Javascript enabled, users can toggle the menu sidebar between "partial fly-out" and "hidden" (it will persist in a client-side cookie). Check here, if you want the initial menu state for those visitors to be "closed", and not default-open.'),
        '#default_value' => $tgs('menu_default_closed'),
      ),
      'menu_hover' => array(
        '#type' => 'checkbox',
        '#title' => t('Enable menu hovering'),
        '#description' => t('If checked, hovering the menu sidebar will also show the expanded menu on desktop sites. Depends also on the global hover settings, see %optionTitle setting in the %menuTitle section.', array(
          '%optionTitle' => t('Hover effects'),
          '%menuTitle' => t('General display'),
        )),
        '#default_value' => $tgs('menu_hover'),
      ),
      'home_icon' => bilderblock::formFileUpload(
        'home_icon',
        t('Home icon'),
        t('The home icon is displayed on minimized menu bars, linking to the home page. It should not exceed 40x40 pixels.')
      ),
    ),
  );

  // Add slide settings.
  $form['theme_settings']['slides'] = array(
    '#weight' => 11,
    '#type' => 'fieldset',
    '#title' => t('Slides'),
    '#description' => t('Configure teaser slide preview images display options.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'theme_settings_tabs',
    'labels' => array(
      '#type' => 'fieldset',
      '#title' => t('Film strip labels'),
      '#description' => t('Turn labels for default node fields on or off.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      '#tree' => TRUE,
      // Options are added @see below.
    ),
    'fallback_image' => bilderblock::formFileUpload(
      'fallback_image',
      t('Fallback slide'),
      t('Fallback images are used as a teaser slide, whenever none of the image fields configured at "Image sources" contains an image in a given node. If you do not like the fallback image shipped with theme, you can upload your own image here. The fallback image is always applied the teaser image style that has been configured for the image field with the highest priority. Make sure it is large enough to cover all of your teaser image styles.')
    ),
    'slide_fx' => array(
      '#type' => 'fieldset',
      '#title' => t('Slide effects'),
      '#description' => t('Additional client-side image processing applied to the "film strip" areas containing teaser slides.'),
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
      'fx_saturation' => array(
        '#type' => 'select',
        '#options' => _bilderblock_options_percent(10, 300, 100),
        '#title' => t('Saturation'),
        '#description' => t("Increase or reduce the original image's saturation. 100% equals the original value, 0 results in greyscale."),
        '#default_value' => $tgs('fx_saturation'),
      ),
      'fx_tint' => array(
        '#type' => 'select',
        '#options' => _bilderblock_options_percent(),
        '#title' => t('Tint'),
        '#description' => t('Whether to add a color overlay to each slide, in the film strip text color, and how opaque this overlay is supposed to be.'),
        '#default_value' => $tgs('fx_tint'),
      ),
      'fx_opacity' => array(
        '#type' => 'select',
        '#options' => _bilderblock_options_percent(5, 100, 100),
        '#title' => t('Opacity'),
        '#description' => t('If set to a value other than 100, slides will be displayed with this opacity unless being hovered. Can be used for e. g. a lightbox effect in combination with hovering.'),
        '#default_value' => $tgs('fx_opacity'),
      ),
      'hover' => array(
        '#type' => 'fieldset',
        '#title' => t('Hover effects'),
        '#description' => t('Instead of solely applying static effects to slide images, the "hover" event can be used to clear all effects from a slide.'),
        'fx_hover_clear' => array(
          '#type' => 'checkbox',
          '#title' => t('Hover clears effects'),
          '#description' => t('If checked, all filters and effects applied to a slide image will be removed when the element is hovered. Also, any filter/effect will be disabled on devices with limited hover capabilities, such as touch. Depends on the global hover settings, see %optionTitle setting in the %menuTitle section.', array(
            '%optionTitle' => t('Hover effects'),
            '%menuTitle' => t('General display'),
          )),
          '#default_value' => $tgs('fx_hover_clear'),
        ),
      ),
    ),
  );

  // Add exposed image settings.
  $always = t('Always');
  $never = t('Never');
  $fallback = t('Only if Javascript is not available on the client');
  $form['theme_settings']['exposed'] = array(
    '#weight' => 12,
    '#type' => 'fieldset',
    '#title' => t('Exposed images'),
    '#description' => t('Configure full node exposed image display options.'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'theme_settings_tabs',
    'toggle_post_image_border' => array(
      '#type' => 'checkbox',
      '#title' => t('Exposed image border'),
      '#description' => t('In full node display, the exposed image is presented outside all other standard blocks. Enable this setting to have it displayed with a border of the color set as above.'),
      '#default_value' => $tgs('toggle_post_image_border'),
    ),
    'zoom_mode' => array(
      '#type' => 'select',
      '#title' => t('Exposed image zoom'),
      '#description' => t('By default, the theme adds a basic CSS-only zoom effect to toggle the exposed image between browser-fit and original size. <ul><li>Choose %always, if you have no other zoom library installed.</li><li>Choose %fallback, if you have a Javascript based zoom library installed.</li><li>Choose %never, if you want no zoom functionality at all.</li></ul>', array(
        '%always' => $always,
        '%never' => $never,
        '%fallback' => $fallback,
      )),
      '#default_value' => $tgs('zoom_mode'),
      '#options' => array(
        bilderblock::ZOOM_ALWAYS => $always,
        bilderblock::ZOOM_FALLBACK => $fallback,
        bilderblock::ZOOM_NEVER => $never,
      ),
    ),
    'disable_exposed' => array(
      '#type' => 'checkbox',
      '#title' => t('Disable exposed image detection'),
      '#description' => t('Check this option if you want to take your own care of what is displayed in the "exposed image" container. The theme will then no longer modify your node content in this regard.'),
      '#default_value' => $tgs('disable_exposed'),
    ),
  );

  // Add label settings.
  $labelDefaults = $tgs('labels');
  foreach(bilderblock::getLabelDefaults() as $key => $label) {
    $form['theme_settings']['slides']['labels'][$key] = array(
      '#type' => 'checkbox',
      '#title' => $label,
      '#description' => t('If enabled, a %label label will be displayed for the corresponding node field. You may also want to disable this whenever your default node fields are already rendered with a different label, e. g. by a view.', array(
        '%label' => $label,
      )),
      '#default_value' => $labelDefaults[$key],
    );
  }

  // "Advanced" section.
  $form['theme_settings']['advanced'] = array(
    '#weight' => 15,
    '#type' => 'fieldset',
    '#title' => t('Advanced settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#group' => 'theme_settings_tabs',
    'menu_cookie' => array(
      '#type' => 'checkbox',
      '#title' => t('Track menu state'),
      '#description' => t('If enabled, a cookie is used to track the menu state (open or closed) in "desktop" and "tablet" landscape display for each client.<ul><li><strong>Enable</strong> for better individual user experience.</li><li><strong>Disable</strong> if your audience is concerned with maximal privacy, or if you have legal impact to consider, or if you do not want to configure a front end cache like Varnish for two different menu states.</li></ul>Also see the "menu" settings in the "general display" section. <strong>Note:</strong> This setting does not refer to the reduced pop-out menu in all other modes, the state of which is always untracked.'),
      '#default_value' => $tgs('menu_cookie'),
    ),
  );

  // Prepare list of image fields.
  $source_fields = array();
  if ($fields = array_filter(field_info_field_map(), function($val) {
    return $val['type'] == 'image';
  })) {
    $defaults = $tgs('source_fields');
    foreach (array_keys($fields) as $idx => $field_name) {
      $default = (array) @$defaults[$field_name];
      $default += array(
        'weight' => 0,
        'active' => 0,
      );
      $source_fields[$field_name] = array(
        '#prefix' => sprintf('<tr class="draggable %s">', ($idx + 1) % 2 == 0 ? 'even' : 'odd'),
        '#suffix' => '</tr>',
        '#weight' => $default['weight'],
        '#parents' => array('source_fields'),
        '#tree' => TRUE,
        'title' => array(
          '#markup' => check_plain($field_name),
          '#prefix' => '<td>',
          '#suffix' => '</td>',
          '#parents' => array('source_fields', $field_name, 'title'),
        ),
        'active' => array(
          '#type' => 'checkbox',
          '#title' => t('Enabled'),
          '#default_value' => $default['active'],
          '#prefix' => '<td>',
          '#suffix' => '</td>',
          '#parents' => array('source_fields', $field_name, 'active'),
        ),
        'weight' => array(
          '#title' => t('Weight'),
          '#type' => 'weight',
          '#default_value' => $default['weight'],
          '#prefix' => '<td class="bilderblock-weight">',
          '#suffix' => '</td>',
          '#parents' => array('source_fields', $field_name, 'weight'),
          '#attributes' => array(
            'class' => array('bilderblock-weight'),
          ),
        ),
      );
    }
  }

  // Add image source mapping.
  $form['theme_settings']['source_fields_wrapper'] = array(
    '#weight' => 14,
    '#type' => 'fieldset',
    '#title' => t('Image sources'),
    '#description' => t("Define which fields are used as \"teaser slide\"/\"exposed image\". To use a field as a source, enable it and optionally set its weight. When a node is displayed, it will be checked for containing the configured fields (those with lower weight prior to the others), and the first field with a match will be used as the source for the node's exposed image."),
    //'#collapsible' => TRUE,
    //'#collapsed' => TRUE,
    '#group' => 'theme_settings_tabs',
    'table_wrapper' => array(
      '#prefix' => '<div><table id="bilderblock-field-source-table" class="sticky-enabled">',
      '#suffix' => '</table></div>',
      'header' => array(
        '#markup' => format_string('<thead><tr><th>!name</th><th>!status</th><th>!weight</th></tr></thead>', array(
          '!name' => t('Field name'),
          '!status' => t('Slide/exposed'),
          '!weight' => t('Weight'),
        )),
      ),
      //'source_fields' => $source_fields,
      //'#tree' => TRUE,
    ) + $source_fields,
  );

  drupal_add_tabledrag('bilderblock-field-source-table', 'order', 'sibling', 'bilderblock-weight');

  // Add submit callback to regenerate dynamic css.
  $form['#validate'][] = 'bilderblock_theme_settings_validate';
}

/**
 * Theme settings validation callback.
 */
function bilderblock_theme_settings_validate(&$form, &$form_state) {
  $v = $form_state['values'];
  // FX speed must be (int) >= 0
  if ($v['fx_speed'] != abs((int) $v['fx_speed'])) {
    form_set_error('fx_speed', t('%value is not a valid effects duration in milliseconds.', array(
      '%value' => $v['fx_speed'],
    )));
  }

  $validators = array('file_validate_is_image' => array());

  // Check for a new uploaded logo.
  foreach (array('home_icon', 'fallback_image') as $childName) {
    $file = file_save_upload("{$childName}_upload", $validators);
    if (isset($file)) {
      // File upload was attempted.
      if ($file) {
        // Put the temporary file in form_values so we can save it on submit.
        $form_state['values']["{$childName}_upload"] = $file;
        $filename = file_unmanaged_copy($file->uri);
        $form_state['values']["{$childName}_path"] = _system_theme_settings_validate_path($filename);
        $form_state['values']["default_{$childName}"] = 0;
      }
      else {
        // File upload failed.
        form_set_error("{$childName}_upload", t('The home icon could not be uploaded.'));
      }
    }
  }
}

/**
 * Generate a percents option array for form elements.
 *
 * @param int $step
 *   (Optional) Positive integer defining the step width. Minimum is 1.
 *   Defaults to 5.
 * @param int $end
 *   (Optional) Positive integer defining the upper scale end.
 *   Defaults to 100.
 * @param mixed $disable_value
 *   (Optional) value in the range from 0 to 100 that will be replaced by
 *   $disable_label in the options label. Set to FALSE to ignore.
 *   Defaults to 0.
 * @param string $disable_label
 *   (Optional) Label that will conditionally replace $disable_value.
 *
 * @return array
 *   Options array.
 */
function _bilderblock_options_percent($step = 5, $end = 100, $disable_value = 0, $disable_label = '---') {
  $start = 0;
  // Steps must be positive integers.
  $step = max(abs((int) $step), 1);
  // Avoid deadlock and force at least one value.
  if ($end <= $start) {
    $end = $start;
  }
  // Init resut array and counter.
  $count = $start;
  $options = array();
  while ($count < $end) {
    $options[$count] = $count;
    $count += $step;
  }
  // Make sure final value is included regardless of step width.
  $options[$end] = $end;
  // Format resulting array for form API.
  foreach ($options as $k => $v) {
    $options[$k] = $disable_value !== FALSE && $v == $disable_value ?
      $disable_label : format_string('!p %', array('!p' => $v));
  };
  return $options;
}
