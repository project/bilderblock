/**
 * @file
 * Javascript extensions for bilderblock Drupal theme.
 */

(function($) {

  Drupal.bilderblock = {

    // Remembers the last content scroll state while the overlay
    // menu is active, to work around some restrictions of the layout.
    scroll: false,
    menuScroll: 0,

    /**
     * Sets a cookie.
     *
     * @param val
     */
    updateCookie: function(val) {
      if (Drupal.settings.bilderblock && Drupal.settings.bilderblock.menuCookie) {
        $.cookie('Drupal.visitor.bilderblockMenu', val, {
          expires: 365,
          path: '/'
        });
      }
    }, // Drupal.bilderblock.cookie
    /**
     * Returns the containing "body" element for the element applied to.
     *
     * @return object
     *   A jQuery result.
     */
    body: function() {
      return $(this).closest('body');
    }, // Drupal.bilderblock.body

    dispatchToggle: function(event) {
      if (Drupal.bilderblock.menu.isLegacy()) {
        Drupal.bilderblock.menu.toggle.call(this, event);
      }
      else {
        Drupal.bilderblock.overlay.toggle.call(this,event);
        Drupal.bilderblock.overlay.tooltip.call(this, event);
      } // Drupal.bilderblock.dispatchToggle
    }, // Drupal.bilderblock
    menu: {
      /**
       * Menu close switch event handler.
       */
      close: function(event) {
        return Drupal.bilderblock.menu.toggle.call(this, event, 'closed');
      }, // Drupal.bilderblock.menu.close
      /**
       * Menu open switch event handler.
       */
      open: function(event) {
        return Drupal.bilderblock.menu.toggle.call(this, event, 'open');
      }, // Drupal.bilderblock.menu.open
      /**
       * Returns whether or not the menu is in legacy ("desktop") mode.
       *
       * @return bool
       */
      isLegacy: function() {
        return $('div#nav-wrapper > div#menu').css('position') == 'fixed';
      },
      /**
       * Returns the target state for toggling the current menu state.
       *
       * Checks the current menu state and returns the opposite state,
       * so the state and the cookie can be changed in sync.
       *
       * @return string
       *   - "closed", if the menu is closed.
       *   - "open" otherwise.
       */
      targetState: function() {
        return Drupal.bilderblock.body.apply(this)
          .hasClass('bilderblock-menu-closed') ? 'open' : 'closed';
      }, // Drupal.bilderblock.menu.targetState
      /**
       * Menu toggle switch event handler.
       *
       * Sets the "bilderblock-menu-closed" control class depending on either
       * the current menu state or an explicit targetState value.
       *
       * @param event
       * @param string targetState
       *   Optional forced target state:
       *   - 'closed'
       *   - 'open'
       *
       * @return object
       *   this
       */
      toggle: function(event, targetState) {
        event.preventDefault();
        var body = Drupal.bilderblock.body.apply(this);
        targetState = targetState || Drupal.bilderblock.menu.targetState.apply(this);
        body.toggleClass('bilderblock-menu-closed', targetState == 'closed');
        // Update button label.
        Drupal.bilderblock.updateCookie(targetState);
        return $(this);
      } // Drupal.bilderblock.menu.toggle
    }, // Drupal.bilderblock.menu
    overlay: {
      /**
       * Determines whether JS menu overlay is currently displayed.
       *
       * @return bool
       */
      state: function() {
        return Drupal.bilderblock.body.apply(this)
          .hasClass('bilderblock-menu-overlay');
      }, // Drupal.bilderblock.overlay.state
      /**
       * Closes the overlay menu without suppressing the original event.
       *
       * Used to extend the "up" button, so it closes an open overlay menu.
       *
       * @return object
       *   this
       */
      closeUp: function(event) {
        event.preventDefault();
        var body = Drupal.bilderblock.body.apply(this);
        // Only update scroll marker if content is currently visible.
        if (!Drupal.bilderblock.menu.isLegacy() && body.hasClass('bilderblock-menu-overlay')) {
          body.removeClass('bilderblock-menu-overlay');
        }
        $(window).scrollTop(0);
        return $(this);
      }, // Drupal.bilderblock.overlay.toggle
      /**
       * Small-screen overlay menu toggle switch event handler.
       *
       * Toggles the "bilderblock-menu-overlay" control body class.
       *
       * @param event
       *
       * @return object
       *   this
       */
      toggle: function(event) {
        event.preventDefault();
        var body = Drupal.bilderblock.body.apply(this);
        // Update scroll marker for currently visible section (menu or content).
        if (!body.hasClass('bilderblock-menu-overlay')) {
          Drupal.bilderblock.scroll = $(window).scrollTop();
        }
        else {
          Drupal.bilderblock.menuScroll = $(window).scrollTop();
        }
        body.toggleClass('bilderblock-menu-overlay');
        Drupal.bilderblock.overlay.tooltip.apply(this);
        // Re-scroll the now open section, if a remembered value exists.
        if (!body.hasClass('bilderblock-menu-overlay')) {
          Drupal.bilderblock.scroll && $(window).scrollTop(Drupal.bilderblock.scroll);
        }
        else {
          // Init menu at total top.
          $(window).scrollTop(Drupal.bilderblock.menuScroll);
        }
        return $(this);
      }, // Drupal.bilderblock.overlay.toggle

      /**
       * Creates a tooltip for menu toggle buttons, depending on menu state.
       *
       * @return string
       */
      tooltip: function() {
        var upTitle = Drupal.t('Jump to content top');
        $(this).closest('body').find('a#bb-top')
          .attr('title', upTitle);
        return $(this);
      } // Drupal.bilderblock.overlay.tooltip
    } // Drupal.bilderblock.overlay
  }; // Drupal.bilderblock

  /**
   * Create our own namespace.
   */
  Drupal.behaviors.bilderblock = {
    attach: function(context) {
      // Bind menu double-click event.
      $('div#nav-wrapper div#navbar', context).once('bilderblock-menu-toggle', function() {
        // Default menu toggle.
        $(this).dblclick(Drupal.bilderblock.dispatchToggle);
      })
      // Add tooltip for double-click:
        .attr('title', Drupal.t('Double-click to toggle the menu, or use the menu icon.'));
      // Bind menu toggle switches events.
      $('a#bb-toggle', context).once('bilderblock-menu-toggle', function() {
        $(this).click(Drupal.bilderblock.dispatchToggle);
      }).attr('title', Drupal.t('Toggle menu'));
      // The "up" botton eventually hides the JS menu and then jumps to the page top.
      $('a#bb-top', context).once('bilderblock-overlay-toggle', function(event) {
        $(this).click(Drupal.bilderblock.overlay.closeUp);
      });
      // Initially hide menu.
      Drupal.bilderblock.body.apply($('div#menu', context)).addClass('bilderblock-menu-has-overlay');
      // Update the cookie with the current value,
      // at least once per page request to reset expiration.
      // (Only if a cookie exists.)
      var c = $.cookie('Drupal.visitor.bilderblockMenu');
      if (c) {
        Drupal.bilderblock.updateCookie(c);
      }
    }
  };

})(jQuery);
